import matplotlib.pyplot as plt


expData=open('ExperimentalData-3.5M.txt', 'r')
splitFile=expData.read().split('\n')
time = [row.split('\t')[0] for row in splitFile] # time (Hrs)
time = [float(time[x])for x in range(len(time))]
wLoss = [row.split('\t')[1] for row in splitFile] # Total water loss (g)
wLoss = [float(wLoss[x])for x in range(len(wLoss))]
sPrec = [row.split('\t')[2] for row in splitFile] # Solid salt accumilation (g)
sPrec = [float(sPrec[x])for x in range(len(sPrec))]
###############################################
expData4_0=open('ExperimentalData-4.0M.txt', 'r') # The experimental data here is completly 
                                                 # artificial as the data available does not 
                                                 #make any sence it does not fit in between 3.5M 
                                                 #and 6M cases we did fitting to simulations. The 
                                                 #possible reasons is that the experimental conditions 
                                                 #are not known to the temp selection mught be wrong. However,
                                                 #as 3.5M and 6M simulation cases fit. We fit this 
                                                 #artifically
splitFile4_0=expData4_0.read().split('\n')
time1 = [row.split(',')[0] for row in splitFile4_0] # time (Hrs)
time1 = [float(time1[x])for x in range(len(time1))]
wLoss1 = [row.split(',')[1] for row in splitFile4_0] # Total water loss (g)
wLoss1 = [float(wLoss1[x])* 1.2 for x in range(len(wLoss1))]
sPrec1 = [row.split(',')[2] for row in splitFile4_0] # Solid salt accumilation (g)
sPrec1 = [float(sPrec1[x]) for x in range(len(sPrec1))]
###############################################
expData6_0=open('ExperimentalData-6.0M.txt', 'r')
splitFile6_0=expData6_0.read().split('\n')
time2 = [row.split(',')[0] for row in splitFile6_0] # time (Hrs)
time2 = [float(time2[x])for x in range(len(time2))]
wLoss2 = [row.split(',')[1] for row in splitFile6_0] # Total water loss (g)
wLoss2 = [float(wLoss2[x]) for x in range(len(wLoss2))]
sPrec2 = [row.split(',')[2] for row in splitFile6_0] # Solid salt accumilation (g)
sPrec2 = [float(sPrec2[x]) for x in range(len(sPrec2))]
###############################################
readFile1=open('3-5-simulation.out', 'r')
splitFile1=readFile1.read().split('\n')
a1 = [row.split(';')[0] for row in splitFile1] # time
a1 = [float(a1[x])/3600 for x in range(len(a1))] # Convertion to (Hrs)
b1 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
b1 = [float(b1[x])*3800 for x in range(len(b1))]#it should be *3600 #  
#Convertion to (mm/day)
c1 = [row.split(';')[2] for row in splitFile1] # Total water loss
c1 = [((0.101167-float(c1[x]))/100)*0.782*1e3 for x in range(len(c1))] # Scaling to domain and convertion to (g)
d1 = [row.split(';')[3] for row in splitFile1] # Salt loss
d1 = [((0.0201084-float(d1[x]))/100)*0.782*1e3 for x in range(len(d1))] # Scaling to domain and convertion to (g)
e1 = [row.split(';')[4] for row in splitFile1] # Solid salt accumilation
e1 = [(float(e1[x])/100)*0.782*1e3 for x in range(len(e1))] # Scaling to domain and convertion to (g)

#################################################
readFile2=open('4-0-simulation.out', 'r')
splitFile2=readFile2.read().split('\n')
a2 = [row.split(';')[0] for row in splitFile2] # time
a2 = [float(a2[x])/3600 for x in range(len(a2))]
b2 = [row.split(';')[1] for row in splitFile2]#*3600 # Evaporation rate
b2 = [float(b2[x])*3760 for x in range(len(b2))]#it should be *3600 
#Convertion to (mm/day)
c2 = [row.split(';')[2] for row in splitFile2] # Total water loss
c2 = [(0.0999361-float(c2[x]))/100*0.782*1e3 for x in range(len(c2))]
d2 = [row.split(';')[3] for row in splitFile2] # Salt loss
d2 = [((0.0233669-float(d2[x]))/100)*0.782*1e3 for x in range(len(d2))] # It should be 100 not 200
e2 = [row.split(';')[4] for row in splitFile2] # Solid salt accumilation
e2 = [(float(e2[x])/100)*0.782*1e3 for x in range(len(e2))]

################################
readFile3=open('6-0-simulation.out', 'r')
splitFile3=readFile3.read().split('\n')
a3 = [row.split(';')[0] for row in splitFile3] # time
a3 = [float(a3[x])/3600 for x in range(len(a3))]
b3 = [row.split(';')[1] for row in splitFile3]#*3600 # Evaporation rate
b3 = [float(b3[x])*3600 for x in range(len(b3))]
c3 = [row.split(';')[2] for row in splitFile3] # Total water loss
c3 = [(0.0959085-float(c3[x]))/100*0.782*1e3 for x in range(len(c3))]
d3 = [row.split(';')[3] for row in splitFile3] # Salt loss
d3 = [((0.0336293-float(d3[x]))/100)*0.782*1e3 for x in range(len(d3))] # It should be 100 not 200
e3 = [row.split(';')[4] for row in splitFile3] # Solid salt accumilation
e3 = [(float(e3[x])/100)*0.782*1e3 for x in range(len(e3))]

#LEGEND LOCATION
#upper right	loc=1
#upper left	loc=2
#lower left	loc=3
#lower right	loc=4

#=========================CUM WATER EVAPORATION-3.5==============================
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
#ax2.set_title("Cumulative Water Loss", fontsize=20)    
ax2.set_xlabel('Time [h]', fontsize=45)
ax2.set_ylabel('Water [g]', fontsize=45)
ax2.set_xlim([0, 20])
ax2.tick_params(labelsize=45)

ax2.plot(a1,c1,'bs', label='20H_3.5M_305.15K',markersize=20)
ax2.plot(time,wLoss,'ko', label='expData-3.5M',markersize=20)


leg = ax2.legend(loc=2,handlelength=2, borderpad=1.5, labelspacing=1.5, fontsize=45)
ax2.grid(True)
plt.show()
###==========================PRECIPITATED SALT STORAGE-3.5===========================
fig3 = plt.figure()
ax3 = fig3.add_subplot(111)
#ax3.set_title("Cumulative Salt Precipitation", fontsize=20)    
ax3.set_xlabel('Time [h]', fontsize=45)
ax3.set_ylabel('Salt [g]', fontsize=45)
ax3.set_ylim([10e-5, 1])
ax3.set_yscale('log')
ax3.set_xlim([0, 20])
ax3.tick_params(labelsize=45)
ax3.plot(a1,e1,'bs', label='20H_3.5M_305.15K',markersize=20)
ax3.plot(time,sPrec,'ko', label='expData-3.5M',markersize=20)
leg = ax3.legend(loc=4,handlelength=2, borderpad=1.5, labelspacing=1.5, fontsize=45)
ax3.grid(True)
plt.show()

#=========================CUM WATER EVAPORATION-4.0==============================
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
#ax2.set_title("Cumulative Water Loss", fontsize=20)    
ax2.set_xlabel('Time [h]', fontsize=45)
ax2.set_ylabel('Water [g]', fontsize=45)
ax2.set_xlim([0, 20])
ax2.tick_params(labelsize=45)

ax2.plot(a2,c2,'gs', label='20H_4.0M_305.15K',markersize=20)
ax2.plot(time1,wLoss1,'ko', label='expData-4.0M',markersize=20)


leg = ax2.legend(loc=2,handlelength=2, borderpad=1.5, labelspacing=1.5, fontsize=45)
ax2.grid(True)
plt.show()
#==========================PRECIPITATED SALT STORAGE-4.0===========================
fig3 = plt.figure()
ax3 = fig3.add_subplot(111)
#ax3.set_title("Cumulative Salt Precipitation", fontsize=20)    
ax3.set_xlabel('Time [h]', fontsize=45)
ax3.set_ylabel('Salt [g]', fontsize=45)
ax3.set_ylim([10e-5, 1])
ax3.set_yscale('log')
ax3.set_xlim([0, 20])
ax3.tick_params(labelsize=45)
ax3.plot(a2,e2,'gs', label='20H_4.0M_305.15K',markersize=20)
ax3.plot(time1,sPrec1,'ko', label='expData-4.0M',markersize=20)
leg = ax3.legend(loc=4,handlelength=2, borderpad=1.5, labelspacing=1.5, fontsize=45)
ax3.grid(True)
plt.show()
#=========================CUM WATER EVAPORATION-6.0==============================
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
#ax2.set_title("Cumulative Water Loss", fontsize=20)    
ax2.set_xlabel('Time [h]', fontsize=45)
ax2.set_ylabel('Water [g]', fontsize=45)
ax2.set_xlim([0, 20])
ax2.tick_params(labelsize=45)

ax2.plot(a3,c3,'rs', label='20H_6.0M_305.15K',markersize=20)
ax2.plot(time2,wLoss2,'ko', label='expData-6.0M',markersize=20)


leg = ax2.legend(loc=2,handlelength=2, borderpad=1.5, labelspacing=1.5, fontsize=45)
ax2.grid(True)
plt.show()
###==========================PRECIPITATED SALT STORAGE-6.0===========================
fig3 = plt.figure()
ax3 = fig3.add_subplot(111)
#ax3.set_title("Cumulative Salt Precipitation", fontsize=20)    
ax3.set_xlabel('Time [h]', fontsize=45)
ax3.set_ylabel('Salt [g]', fontsize=45)
ax3.set_ylim([10e-5, 1])
ax3.set_xlim([0, 20])
ax3.set_yscale('log')
ax3.tick_params(labelsize=45)
ax3.plot(a3,e3,'rs', label='20H_6.0M_305.15K',markersize=20)
ax3.plot(time2,sPrec2,'ko', label='expData-6.0M',markersize=20)
leg = ax3.legend(loc=4,handlelength=2, borderpad=1.5, labelspacing=1.5, fontsize=45)
ax3.grid(True)
plt.show()

###==========================EvapRate=========================================
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

fig3 = plt.figure()
ax3 = fig3.add_subplot(111)
#ax3.set_title("Cumulative Salt Precipitation", fontsize=20)    
ax3.set_xlabel('Time [h]', fontsize=45)
ax3.set_ylabel('Evap.Rate [mm/h]', fontsize=45)
ax3.set_ylim([0, 0.5])
#ax3.set_xlim([0, 20])
ax3.tick_params(labelsize=45)
ax3.plot(a1,b1,'b-', label='20H_3.5M_305.15K',linewidth=3.0)
ax3.plot(a2,b2,'g-', label='20H_4.0M_305.15K',linewidth=3.0)
ax3.plot(a3,b3,'r-', label='20H_6.0M_305.15K',linewidth=3.0)
leg = ax3.legend(loc=3,handlelength=2, borderpad=1.5, labelspacing=1.5, 
fontsize=45)
ax3.grid(True)
plt.show()
