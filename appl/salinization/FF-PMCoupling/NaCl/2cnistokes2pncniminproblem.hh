/*****************************************************************************
 *   Copyright (C) 2013 by Vishal Jambhekar                                  *
 *   Copyright (C) 2012 by Klaus Mosthaf                                     *
 *   Copyright (C) 2009 by Bernd Flemisch                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_2CNISTOKES_2PNCNIMINPROBLEM_HH
#define DUMUX_2CNISTOKES_2PNCNIMINPROBLEM_HH

#define SALINIZATION 1

#include <dune/grid/common/gridinfo.hh>
#include <dune/grid/multidomaingrid.hh>
#include <dune/grid/io/file/dgfparser.hh>

#include <dumux/multidomain/common/multidomainproblem.hh>
//#include <dumux/modelcoupling/common/multidomainassembler.hh>
#include <dumux/modelcoupling/2cnistokes2pncnimin/2cnistokes2pncniminlocaloperator.hh>
#include <dumux/modelcoupling/2cstokes2pncmin/2cstokes2pncminnewtoncontroller.hh>

#include <dumux/linear/seqsolverbackend.hh>

//#include <dumux/material/fluidsystems/h2oairfluidsystem.hh>
#include <dumux/material/fluidsystems/brineairfluidsystem.hh>
#include "2cnistokes2pncniminspatialparams.hh"
#include "stokes2cnisubproblem.hh"
#include "2pncniminsubproblem.hh"

namespace Dumux
{
template <class TypeTag>
class TwoCNIStokesTwoPNCNIMinProblem;

namespace Properties
{
NEW_TYPE_TAG(TwoCNIStokesTwoPNCNIMinProblem, INHERITS_FROM(MultiDomain));

// Set the grid type
SET_PROP(TwoCNIStokesTwoPNCNIMinProblem, Grid)
{
 public:
#ifdef HAVE_UG
    typedef typename Dune::UGGrid<2> type;
#elif HAVE_ALUGRID
    typedef typename Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming> type;
#else
#error Required UG or ALUGrid.
#endif
};

// Specify the multidomain gridview
SET_PROP(TwoCNIStokesTwoPNCNIMinProblem, GridView)
{
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MDGrid;
 public:
    typedef typename MDGrid::LeafGridView type;
};

// Set the global problem
SET_TYPE_PROP(TwoCNIStokesTwoPNCNIMinProblem, Problem, TwoCNIStokesTwoPNCNIMinProblem<TypeTag>);

// Set the two sub-problems of the global problem
SET_TYPE_PROP(TwoCNIStokesTwoPNCNIMinProblem, SubDomain1TypeTag, TTAG(Stokes2cniSubProblem));
SET_TYPE_PROP(TwoCNIStokesTwoPNCNIMinProblem, SubDomain2TypeTag, TTAG(TwoPNCNIMinSubProblem));

// Set the global problem in the context of the two sub-problems
SET_TYPE_PROP(Stokes2cniSubProblem, MultiDomainTypeTag, TTAG(TwoCNIStokesTwoPNCNIMinProblem));
SET_TYPE_PROP(TwoPNCNIMinSubProblem, MultiDomainTypeTag, TTAG(TwoCNIStokesTwoPNCNIMinProblem));

// Set the other sub-problem for each of the two sub-problems
SET_TYPE_PROP(Stokes2cniSubProblem, OtherSubDomainTypeTag, TTAG(TwoPNCNIMinSubProblem));
SET_TYPE_PROP(TwoPNCNIMinSubProblem, OtherSubDomainTypeTag, TTAG(Stokes2cniSubProblem));

// Set the same spatial parameters for both sub-problems
SET_PROP(Stokes2cniSubProblem, SpatialParams)
{ typedef Dumux::TwoCNIStokesTwoPNCNIMinSpatialParams<TypeTag> type; };
SET_PROP(TwoPNCNIMinSubProblem, SpatialParams)
{ typedef Dumux::TwoCNIStokesTwoPNCNIMinSpatialParams<TypeTag> type; };

// Set the fluid system
SET_PROP(TwoCNIStokesTwoPNCNIMinProblem, FluidSystem)

{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dumux::FluidSystems::BrineAir<Scalar,
            Dumux::H2O<Scalar>,
            /*useComplexrelations=*/true> type;
};

SET_TYPE_PROP(TwoCNIStokesTwoPNCNIMinProblem, JacobianAssembler, Dumux::MultiDomainAssembler<TypeTag>);
SET_TYPE_PROP(TwoCNIStokesTwoPNCNIMinProblem, MultiDomainCouplingLocalOperator, Dumux::TwoCNIStokesTwoPNCNIMinLocalOperator<TypeTag>);

// newton parameters
SET_TYPE_PROP(TwoCNIStokesTwoPNCNIMinProblem, NewtonController, Dumux::TwoCStokesTwoPNCMinNewtonController<TypeTag>);

SET_TYPE_PROP(TwoCNIStokesTwoPNCNIMinProblem, LinearSolver, SuperLUBackend<TypeTag>);
// set this to one here (must fit to the structure of the coupled matrix which has block length 1)
SET_INT_PROP(TwoCNIStokesTwoPNCNIMinProblem, NumEq, 1);

SET_PROP(TwoCNIStokesTwoPNCNIMinProblem, SolutionVector)
{
 private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGridOperator) MDGridOperator;
 public:
    typedef typename MDGridOperator::Traits::Domain type;
};

// Write the solutions of individual newton iterations?
SET_BOOL_PROP(TwoCNIStokesTwoPNCNIMinProblem, NewtonWriteConvergence, false);

}

/*!
 * \brief The problem class for the coupling of an isothermal two-component Stokes
 *        and an isothermal two-phase two-component Darcy model.
 *
 *        The problem class for the coupling of an isothermal two-component Stokes (stokes2c)
 *        and an isothermal two-phase two-component Darcy model (2p2c).
 *        It uses the 2p2cCoupling model and the Stokes2ccoupling model and provides
 *        the problem specifications for common parameters of the two submodels.
 *        The initial and boundary conditions of the submodels are specified in the two subproblems,
 *        2p2csubproblem.hh and Stokes2cniSubProblem.hh, which are accessible via the coupled problem.
 */
template <class TypeTag = TTAG(TwoCNIStokesTwoPNCNIMinProblem)>
class TwoCNIStokesTwoPNCNIMinProblem : public MultiDomainProblem<TypeTag>
{
    template<int dim>
    struct VertexLayout
    {
        bool contains(Dune::GeometryType gt) {
            return gt.dim() == 0;
        }
    };

    typedef TwoCNIStokesTwoPNCNIMinProblem<TypeTag> ThisType;
    typedef MultiDomainProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;
    typedef typename GET_PROP_TYPE(TypeTag, NewtonController) NewtonController;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) Stokes2cniTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) TwoPNCMinTypeTag;

    typedef typename GET_PROP_TYPE(Stokes2cniTypeTag, Problem) Stokes2cniSubProblem;
    typedef typename GET_PROP_TYPE(TwoPNCMinTypeTag, Problem) TwoPNCNIMinSubProblem;

    typedef typename GET_PROP_TYPE(Stokes2cniTypeTag, Model) Stokes2cniModel;
    typedef typename GET_PROP_TYPE(TwoPNCMinTypeTag, Model) TwoPNCMinModel;

    typedef typename GET_PROP_TYPE(Stokes2cniTypeTag, GridView) Stokes2cniGridView;
    typedef typename GET_PROP_TYPE(TwoPNCMinTypeTag, GridView) TwoPNCMinGridView;

    typedef typename GET_PROP_TYPE(Stokes2cniTypeTag, PrimaryVariables) Stokes2cniPrimaryVariables;
    typedef typename GET_PROP_TYPE(TwoPNCMinTypeTag, PrimaryVariables) TwoPNCMinPrimaryVariables;

    typedef typename GET_PROP_TYPE(Stokes2cniTypeTag, ElementSolutionVector) ElementSolutionVector1;
    typedef typename GET_PROP_TYPE(TwoPNCMinTypeTag, ElementSolutionVector) ElementSolutionVector2;

    typedef typename GET_PROP_TYPE(Stokes2cniTypeTag, ElementVolumeVariables) ElementVolumeVariables1;
    typedef typename GET_PROP_TYPE(TwoPNCMinTypeTag, ElementVolumeVariables) ElementVolumeVariables2;

    typedef typename GET_PROP_TYPE(Stokes2cniTypeTag, FluxVariables) BoundaryVariables1;

    typedef typename GET_PROP_TYPE(Stokes2cniTypeTag, BoundaryTypes) BoundaryTypes1;
    typedef typename GET_PROP_TYPE(TwoPNCMinTypeTag, BoundaryTypes) BoundaryTypes2;

    typedef typename GET_PROP_TYPE(Stokes2cniTypeTag, FVElementGeometry) FVElementGeometry1;
    typedef typename GET_PROP_TYPE(TwoPNCMinTypeTag, FVElementGeometry) FVElementGeometry2;

//    typedef typename GET_PROP_TYPE(TypeTag, Grid) HostGrid;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MDGrid;
    typedef typename MDGrid::LeafGridView MDGridView;

    typedef typename Stokes2cniGridView::template Codim<0>::Entity SDElement1;
    typedef typename TwoPNCMinGridView::template Codim<0>::Entity SDElement2;

    typedef typename MDGrid::Traits::template Codim<0>::EntityPointer MDElementPointer;
    typedef typename MDGrid::SubDomainGrid SDGrid;
    typedef typename SDGrid::template Codim<0>::EntityPointer SDElementPointer;
    typedef typename MDGrid::Traits::template Codim<0>::Entity MDElement;

    typedef typename GET_PROP_TYPE(Stokes2cniTypeTag, Indices) Stokes2cniIndices;
    typedef typename GET_PROP_TYPE(TwoPNCMinTypeTag, Indices) TwoPNCMinIndices;

    typedef typename GET_PROP_TYPE(Stokes2cniTypeTag, FluidSystem) FluidSystem;

    enum { dim = Stokes2cniGridView::dimension };
	enum { // indices in the Stokes domain
        momentumXIdx1 = Stokes2cniIndices::momentumXIdx, //!< Index of the x-component of the momentum balance
        momentumYIdx1 = Stokes2cniIndices::momentumYIdx, //!< Index of the y-component of the momentum balance
        momentumZIdx1 = Stokes2cniIndices::momentumZIdx, //!< Index of the z-component of the momentum balance
        lastMomentumIdx1 = Stokes2cniIndices::lastMomentumIdx, //!< Index of the last component of the momentum balance
        massBalanceIdx1 = Stokes2cniIndices::massBalanceIdx, //!< Index of the mass balance
        transportEqIdx1 = Stokes2cniIndices::transportEqIdx, //!< Index of the transport equation
	};
	enum { // indices in the Darcy domain
        massBalanceIdx2 = TwoPNCMinIndices::pressureIdx,
        switchIdx2 = TwoPNCMinIndices::switchIdx,
        contiTotalMassIdx2 = TwoPNCMinIndices::contiNEqIdx,
        contiWEqIdx2 = TwoPNCMinIndices::contiWEqIdx,

        wCompIdx2 = FluidSystem::wCompIdx,
        nCompIdx2 = FluidSystem::nCompIdx,

        wPhaseIdx2 = TwoPNCMinIndices::wPhaseIdx,
        nPhaseIdx2 = TwoPNCMinIndices::nPhaseIdx

    };
    enum { transportCompIdx1 = Stokes2cniIndices::transportCompIdx };
    enum { phaseIdx = GET_PROP_VALUE(Stokes2cniTypeTag, PhaseIdx) };
    enum {
        numEq1 = GET_PROP_VALUE(Stokes2cniTypeTag, NumEq),
        numEq2 = GET_PROP_VALUE(TwoPNCMinTypeTag, NumEq)
    };

    typedef Dune::FieldVector<Scalar, dim> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> FieldVector;

    ////////////
    typedef typename MDGrid::template Codim<0>::LeafIterator ElementIterator;
    typedef typename MDGrid::LeafSubDomainInterfaceIterator SDInterfaceIterator;
    typedef typename MDGridView::IndexSet::IndexType IndexType;
    ////////////

public:
    TwoCNIStokesTwoPNCNIMinProblem(MDGrid &mdGrid,
                              	  TimeManager &timeManager)
    : ParentType(mdGrid, timeManager)
    {
        try
        {
            // define location of the interface
            interface_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
            noDarcyX_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, NoDarcyX);
            episodeLength_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, EpisodeLength);
            initializationTime_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);

            // define output options
            freqRestart_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqRestart);
            freqOutput_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqOutput);
            freqFluxOutput_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqFluxOutput);
            freqVaporFluxOutput_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqVaporFluxOutput);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }

        stokes2cni_ = this->sdID1();
        twoPNCNIMin_ = this->sdID2();

        initializeGrid();

        // initialize the tables of the fluid system
        FluidSystem::init();

        if (initializationTime_ > 0.0)
            this->timeManager().startNextEpisode(initializationTime_);
        else
            this->timeManager().startNextEpisode(episodeLength_);
    }

    ~TwoCNIStokesTwoPNCNIMinProblem()
    {
        fluxFile_.close();
    };

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     */
    void init()
    {
        ParentType::init();

        // plot the Pc-Sw curves, if requested
        this->sdProblem2().spatialParams().plotMaterialLaw();

        std::cout << "Writing flux data at interface\n";
        if (this->timeManager().time() == 0)
        {
            fluxFile_.open("fluxes.out");
            fluxFile_ << "time; flux1; advFlux1; diffFlux1; totalFlux1; flux2; wPhaseFlux2; nPhaseFlux2\n";
            counter_ = 1;
        }
        else
            fluxFile_.open("fluxes.out", std::ios_base::app);
    }

    void initializeGrid()
    {
        MDGrid& mdGrid = this->mdGrid();
        mdGrid.startSubDomainMarking();

        // subdivide grid in two subdomains
        ElementIterator eendit = mdGrid.template leafend<0>();
        for (ElementIterator elementIt = mdGrid.template leafbegin<0>();
             elementIt != eendit; ++elementIt)
        {
            // this is required for parallelization
            // checks if element is within a partition
            if (elementIt->partitionType() != Dune::InteriorEntity)
                continue;

            GlobalPosition globalPos = elementIt->geometry().center();

            if (globalPos[1] > interface_)
                mdGrid.addToSubDomain(stokes2cni_,*elementIt);
            else
                if(globalPos[0] > noDarcyX_)
                    mdGrid.addToSubDomain(twoPNCNIMin_,*elementIt);
        }
        mdGrid.preUpdateSubDomains();
        mdGrid.updateSubDomains();
        mdGrid.postUpdateSubDomains();

        gridinfo(this->sdGrid1());
        gridinfo(this->sdGrid2());

        this->sdProblem1().spatialParams().loadIntrinsicPermeability(this->sdGridView1());
        this->sdProblem2().spatialParams().loadIntrinsicPermeability(this->sdGridView2());
    }

    /*!
     * \brief Called by the time manager after the time integration.
     */
    void postTimeStep()
    {
        // call the postTimeStep function of the subproblems
        this->sdProblem1().postTimeStep();
        this->sdProblem2().postTimeStep();

        if (shouldWriteFluxFile() || shouldWriteVaporFlux())
        {
            counter_ = this->sdProblem1().currentVTKFileNumber() + 1;

//            calculateFirstInterfaceFluxes();
//            calculateSecondInterfaceFluxes();
        }
    }

    /*!
     * \brief Called by the time manager after the end of an episode.
     */
    void episodeEnd()
    { this->timeManager().startNextEpisode(episodeLength_); }

    /*
     * \brief Calculates fluxes and coupling terms at the interface for the Stokes model.
     *        Flux output files are created and the summarized flux is written to a file.
     */
    void calculateFirstInterfaceFluxes()
    {
        const MDGrid& mdGrid = this->mdGrid();
        ElementVolumeVariables1 elemVolVarsPrev1, elemVolVarsCur1;
        Scalar sumVaporFluxes = 0.;
        Scalar advectiveVaporFlux = 0.;
        Scalar diffusiveVaporFlux = 0.;

        // count number of elements to determine number of interface nodes
        int numElements = 0;
        const SDInterfaceIterator endIfIt = mdGrid.leafSubDomainInterfaceEnd(stokes2cni_, twoPNCNIMin_);
        for (SDInterfaceIterator ifIt =
                 mdGrid.leafSubDomainInterfaceBegin(stokes2cni_, twoPNCNIMin_); ifIt != endIfIt; ++ifIt)
            numElements++;

        const int numInterfaceVertices = numElements + 1;
        std::vector<InterfaceFluxes<numEq1> > outputVector(numInterfaceVertices); // vector for the output of the fluxes
        FVElementGeometry1 fvGeometry1;
        int interfaceVertIdx = -1;

        // loop over faces on the interface
        for (SDInterfaceIterator ifIt =
                 mdGrid.leafSubDomainInterfaceBegin(stokes2cni_, twoPNCNIMin_); ifIt != endIfIt; ++ifIt)
        {
            const int firstFaceIdx = ifIt->indexInFirstCell();
            MDElementPointer mdElementPointer1 = ifIt->firstCell(); // ATTENTION!!!
            const MDElement& mdElement1 = *(mdElementPointer1);     // Entity pointer has to be copied before.
            const SDElement1& sdElement1 = *(this->sdElementPointer1(mdElement1));
            fvGeometry1.update(this->gridView1(), sdElement1);

            const Dune::GenericReferenceElement<typename MDGrid::ctype,dim>& referenceElement1 =
                Dune::GenericReferenceElements<typename MDGrid::ctype,dim>::general(mdElement1.type());
            const int numVerticesOfFace = referenceElement1.size(firstFaceIdx, 1, dim);

            // evaluate residual of the sub model without boundary conditions (stabilization is removed)
            // the element volume variables are updated here
            this->localResidual1().evalNoBoundary(sdElement1, fvGeometry1,
                                                  elemVolVarsPrev1, elemVolVarsCur1);

            for (int nodeInFace = 0; nodeInFace < numVerticesOfFace; nodeInFace++)
            {
                const int vertInElem1 = referenceElement1.subEntity(firstFaceIdx, 1, nodeInFace, dim);
                const FieldVector& vertexGlobal = mdElement1.geometry().corner(vertInElem1);
                const int firstGlobalIdx = this->mdVertexMapper().map(stokes2cni_, mdElement1, vertInElem1, dim);
                const ElementSolutionVector1& firstVertexDefect = this->localResidual1().residual();

                // loop over all interface vertices to check if vertex id is already in stack
                bool existing = false;
                for (int interfaceVertex=0; interfaceVertex < numInterfaceVertices; ++interfaceVertex)
                {
                    if (firstGlobalIdx == outputVector[interfaceVertex].globalIdx)
                    {
                        existing = true;
                        interfaceVertIdx = interfaceVertex;
                        break;
                    }
                }

                if (!existing)
                    interfaceVertIdx++;

                if (shouldWriteFluxFile()) // compute only if required
                {
                    outputVector[interfaceVertIdx].interfaceVertex = interfaceVertIdx;
                    outputVector[interfaceVertIdx].globalIdx = firstGlobalIdx;
                    outputVector[interfaceVertIdx].xCoord = vertexGlobal[0];
                    outputVector[interfaceVertIdx].yCoord = vertexGlobal[1];
                    outputVector[interfaceVertIdx].count += 1;
                    for (int eqIdx=0; eqIdx < numEq1; ++eqIdx)
                        outputVector[interfaceVertIdx].defect[eqIdx] +=
                            firstVertexDefect[vertInElem1][eqIdx];
                }

                int boundaryFaceIdx =
                    fvGeometry1.boundaryFaceIndex(firstFaceIdx, nodeInFace);

                const BoundaryVariables1 boundaryVars1(this->subProblem1(),
                                                       sdElement1,
                                                       fvGeometry1,
                                                       boundaryFaceIdx,
                                                       elemVolVarsCur1,
                                                       /*onBoundary=*/true);

                advectiveVaporFlux += computeAdvectiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem1);
                diffusiveVaporFlux += computeDiffusiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem1);
                sumVaporFluxes += firstVertexDefect[vertInElem1][transportEqIdx1];
            }
        }

        if (shouldWriteFluxFile())
        {
            std::cout << "Writing flux file\n";
            char outputname[20];
            sprintf(outputname, "%s%05d%s","fluxes1_", counter_,".out");
            std::ofstream outfile(outputname, std::ios_base::out);
            outfile << "Xcoord1 "
                    << "totalFlux1 "
                    << "componentFlux1 "
//                    << "momentumXFlux1 "
//                    << "momentumYFlux1 "
                    << "count1 "
                    << std::endl;
            for (int interfaceVertIdx=0; interfaceVertIdx < numInterfaceVertices; interfaceVertIdx++)
            {
                //            if (outputVector[interfaceVertIdx].count == 1)
                //            {
                //                outputVector[interfaceVertIdx].rhoV *= 2;
                //                outputVector[interfaceVertIdx].normalForce *= 2;
                //            }
                if (outputVector[interfaceVertIdx].count > 2)
                    std::cerr << "too often at one node!!";

                if (outputVector[interfaceVertIdx].count==2)
                    outfile << outputVector[interfaceVertIdx].xCoord << " "
                            << outputVector[interfaceVertIdx].defect[massBalanceIdx1] << " " // total mass flux
                            << outputVector[interfaceVertIdx].defect[transportEqIdx1] << " " // total flux of component
//                            << outputVector[interfaceVertIdx].defect[momentumXIdx1] << " " // total flux of momentum X-direction
//                            << outputVector[interfaceVertIdx].defect[momentumYIdx1] << " " // total flux of momentum Y-direction
//                            << outputVector[interfaceVertIdx].interfaceVertex << " "
//                            << outputVector[interfaceVertIdx].globalIdx << " "
//                            << outputVector[interfaceVertIdx].yCoord << " "
                            << outputVector[interfaceVertIdx].count << " "
                            << std::endl;
            }
            outfile.close();
        }

        if (this->timeManager().time() != 0.)
            fluxFile_ << this->timeManager().time() + this->timeManager().timeStepSize() << "; "
                      << sumVaporFluxes << "; " << advectiveVaporFlux << "; " << diffusiveVaporFlux << "; "
                      << advectiveVaporFlux-diffusiveVaporFlux << "; ";

        return;
    }

    /*
     * \brief Calculates fluxes and coupling terms at the interface for the Darcy model
     *        and creates output files
     */
    void calculateSecondInterfaceFluxes()
    {
        const MDGrid& mdGrid = this->mdGrid();
        ElementVolumeVariables2 elemVolVarsPrev2, elemVolVarsCur2;

        Scalar sumVaporFluxes = 0.;
        Scalar sumWaterFluxInGasPhase = 0.;

        // count number of elements to determine number of interface nodes
        int numElements = 0;
        const SDInterfaceIterator endIfIt = mdGrid.leafSubDomainInterfaceEnd(stokes2cni_, twoPNCNIMin_);
        for (SDInterfaceIterator ifIt =
                 mdGrid.leafSubDomainInterfaceBegin(stokes2cni_, twoPNCNIMin_); ifIt != endIfIt; ++ifIt)
            numElements++;

        const int numInterfaceVertices = numElements + 1;
        std::vector<InterfaceFluxes<numEq2> > outputVector(numInterfaceVertices); // vector for the output of the fluxes
        FVElementGeometry2 fvGeometry2;
        int interfaceVertIdx = -1;

        // loop over faces on the interface
        for (SDInterfaceIterator ifIt =
                 mdGrid.leafSubDomainInterfaceBegin(stokes2cni_, twoPNCNIMin_); ifIt != endIfIt; ++ifIt)
        {
            const int secondFaceIdx = ifIt->indexInSecondCell();
            MDElementPointer mdElementPointer2 = ifIt->secondCell(); // ATTENTION!!!
            const MDElement& mdElement2 = *(mdElementPointer2);     // Entity pointer has to be copied before.
            const SDElement2& sdElement2 = *(this->sdElementPointer2(mdElement2));
            fvGeometry2.update(this->gridView2(), sdElement2);

            const Dune::GenericReferenceElement<typename MDGrid::ctype,dim>& referenceElement2 =
                Dune::GenericReferenceElements<typename MDGrid::ctype,dim>::general(mdElement2.type());
            const int numVerticesOfFace = referenceElement2.size(secondFaceIdx, 1, dim);

            // evaluate residual of the sub model without boundary conditions
            this->localResidual2().evalNoBoundary(sdElement2, fvGeometry2,
                                                  elemVolVarsPrev2, elemVolVarsCur2);
            // evaluate the vapor fluxes within each phase
            this->localResidual2().evalPhaseFluxes();

            for (int nodeInFace = 0; nodeInFace < numVerticesOfFace; nodeInFace++)
            {
                const int vertInElem2 = referenceElement2.subEntity(secondFaceIdx, 1, nodeInFace, dim);
                const FieldVector& vertexGlobal = mdElement2.geometry().corner(vertInElem2);
                const int secondGlobalIdx = this->mdVertexMapper().map(twoPNCNIMin_,mdElement2,vertInElem2,dim);
                const ElementSolutionVector2& secondVertexDefect = this->localResidual2().residual();

                bool existing = false;
                // loop over all interface vertices to check if vertex id is already in stack
                for (int interfaceVertex=0; interfaceVertex < numInterfaceVertices; ++interfaceVertex)
                {
                    if (secondGlobalIdx == outputVector[interfaceVertex].globalIdx)
                    {
                        existing = true;
                        interfaceVertIdx = interfaceVertex;
                        break;
                    }
                }

                if (!existing)
                    interfaceVertIdx++;

                if (shouldWriteFluxFile())
                {
                    outputVector[interfaceVertIdx].interfaceVertex = interfaceVertIdx;
                    outputVector[interfaceVertIdx].globalIdx = secondGlobalIdx;
                    outputVector[interfaceVertIdx].xCoord = vertexGlobal[0];
                    outputVector[interfaceVertIdx].yCoord = vertexGlobal[1];
                    for (int eqIdx=0; eqIdx < numEq2; ++eqIdx)
                        outputVector[interfaceVertIdx].defect[eqIdx] += secondVertexDefect[vertInElem2][eqIdx];
                    outputVector[interfaceVertIdx].count += 1;
                }
                if (!existing) // add phase storage only once per vertex
                    sumWaterFluxInGasPhase +=
                        this->localResidual2().evalPhaseStorage(vertInElem2);

                sumVaporFluxes += secondVertexDefect[vertInElem2][contiWEqIdx2];
                sumWaterFluxInGasPhase +=
                    this->localResidual2().elementFluxes(vertInElem2);
            }
        }

        if (shouldWriteFluxFile())
        {
            char outputname[20];
            sprintf(outputname, "%s%05d%s","fluxes2_", counter_,".out");
            std::ofstream outfile(outputname, std::ios_base::out);
            outfile << "Xcoord2 "
                    << "totalFlux2 "
                    << "componentFlux2 "
//                    << "count2 "
                    << std::endl;

            for (int interfaceVertIdx=0; interfaceVertIdx < numInterfaceVertices; interfaceVertIdx++)
            {
                //            if (outputVector[interfaceVertIdx].count == 1)
                //            {
                //                outputVector[interfaceVertIdx].rhoV *= 2;
                //                outputVector[interfaceVertIdx].normalForce *= 2;
                //            }
                if (outputVector[interfaceVertIdx].count > 2)
                    std::cerr << "too often at one node!!";

                if (outputVector[interfaceVertIdx].count==2)
                    outfile << outputVector[interfaceVertIdx].xCoord << " "
                            << outputVector[interfaceVertIdx].defect[contiTotalMassIdx2] << " " // total mass flux
                            << outputVector[interfaceVertIdx].defect[contiWEqIdx2] << " " // total flux of component
//                            << outputVector[interfaceVertIdx].interfaceVertex << " "
//                            << outputVector[interfaceVertIdx].globalIdx << " "
//                            << outputVector[interfaceVertIdx].yCoord << " "
//                            << outputVector[interfaceVertIdx].count << " "
                            << std::endl;
            }
            outfile.close();
        }
        if (this->timeManager().time() != 0.){
            Scalar sumWaterFluxInLiquidPhase = sumVaporFluxes - sumWaterFluxInGasPhase;
            fluxFile_ << sumVaporFluxes << "; "
                      << sumWaterFluxInLiquidPhase << "; "
                      << sumWaterFluxInGasPhase << std::endl;
        }
        return;
    }

    Scalar computeAdvectiveVaporFluxes1(const ElementVolumeVariables1& elemVolVars1,
                                        const BoundaryVariables1& boundaryVars1,
                                        int vertInElem1)
    {
        Scalar advFlux = elemVolVars1[vertInElem1].density() *
            elemVolVars1[vertInElem1].fluidState().massFraction(phaseIdx, transportCompIdx1) *
            boundaryVars1.normalVelocity();
        return advFlux;
    }

    Scalar computeDiffusiveVaporFluxes1(const ElementVolumeVariables1& elemVolVars1,
                                        const BoundaryVariables1& boundaryVars1,
                                        int vertInElem1)
    {
        Scalar diffFlux = boundaryVars1.moleFractionGrad() *
            boundaryVars1.face().normal *
            boundaryVars1.diffusionCoeff() *
            boundaryVars1.molarDensity() *
            FluidSystem::molarMass(transportCompIdx1);
        return diffFlux;
    }

    /*!
     * \brief Returns true if a restart file should be written to disk.
     */
    bool shouldWriteRestartFile() const
    {
        if ((this->timeManager().timeStepIndex() > 0 &&
             (this->timeManager().timeStepIndex() % freqRestart_ == 0))
            // also write a restart file at the end of each episode
            || this->timeManager().episodeWillBeOver())
            return true;
        return false;
    }


    /*!
     * \brief Returns true if a VTK output file should be written.
     */
    bool shouldWriteOutput() const
    {
        if (this->timeManager().timeStepIndex() % freqOutput_ == 0
            || this->timeManager().episodeWillBeOver())
            return true;
        return false;
    }

    /*!
     * \brief Returns true if a file with the fluxes across the
     * free-flow -- porous-medium interface should be written.
     */
    bool shouldWriteFluxFile() const
    {
        if (this->timeManager().timeStepIndex() % freqFluxOutput_ == 0
            || this->timeManager().episodeWillBeOver())
            return true;
        return false;
    }

    /*!
     * \brief Returns true if the summarized vapor fluxes
     *        across the free-flow -- porous-medium interface,
     *        representing the evaporation rate (related to the
     *        interface area), should be written.
     */
    bool shouldWriteVaporFlux() const
    {
        if (this->timeManager().timeStepIndex() % freqVaporFluxOutput_ == 0
            || this->timeManager().episodeWillBeOver())
            return true;
        return false;

    }

    Stokes2cniSubProblem& stokes2cProblem()
    { return this->subProblem1(); }
    const Stokes2cniSubProblem& stokes2cProblem() const
    { return this->subProblem1(); }

    TwoPNCNIMinSubProblem& twoPNCMinProblem()
    { return this->subProblem2(); }
    const TwoPNCNIMinSubProblem& twoPNCMinProblem() const
    { return this->subProblem2(); }

private:
    typename MDGrid::SubDomainType stokes2cni_;
    typename MDGrid::SubDomainType twoPNCNIMin_;

    unsigned counter_;
    unsigned freqRestart_;
    unsigned freqOutput_;
    unsigned freqFluxOutput_;
    unsigned freqVaporFluxOutput_;

    Scalar interface_;
    Scalar episodeLength_;
    Scalar noDarcyX_;
    Scalar initializationTime_;

    template <int numEq>
    struct InterfaceFluxes
    {
        unsigned count;
        unsigned interfaceVertex;
        unsigned globalIdx;
        Scalar xCoord;
        Scalar yCoord;
        Dune::FieldVector<Scalar, numEq> defect;

        InterfaceFluxes()
        {
            count = 0;
            interfaceVertex = 0;
            globalIdx = 0;
            xCoord = 0.0;
            yCoord = 0.0;
            defect = 0.0;
        }
    };
    std::ofstream fluxFile_;
};

} //end namespace

#endif
