import matplotlib.pyplot as plt

################### 0.5 M ###########################################################
expData=open('ExperimentalData-0.5M.txt', 'r')
splitFile=expData.read().split('\n')
time = [row.split('\t')[0] for row in splitFile] # time (Hrs)
time = [float(time[x])for x in range(len(time))]
wLoss = [row.split('\t')[1] for row in splitFile] # Total water loss (g)
wLoss = [float(wLoss[x])for x in range(len(wLoss))]
evapRate = [row.split('\t')[2] for row in splitFile] # Solid salt accumilation (g)
evapRate = [float(evapRate[x])for x in range(len(evapRate))]
######################################################################################
readFile1=open('withoutThermalDiff-F-0.02.out', 'r')
splitFile1=readFile1.read().split('\n')
a1 = [row.split(';')[0] for row in splitFile1] # time
a1 = [float(a1[x])/86400 for x in range(len(a1))] # Convertion to (Hrs)
b1 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
b1 = [float(b1[x])*86400 for x in range(len(b1))]# Convertion to (mm/day)
c1 = [row.split(';')[2] for row in splitFile1] # Total water loss
c1 = [(float (c1[0])-float(c1[x]))*0.065*1e3 for x in range(len(c1))] #Scaling to domain and convertion to (g)
d1 = [row.split(';')[3] for row in splitFile1] # Salt loss
d1 = [(float(d1[0])-float(d1[x]))*0.065*1e3 for x in range(len(d1))] #Scaling to domain and convertion to (g)
e1 = [row.split(';')[4] for row in splitFile1] # Solid salt accumilation
e1 = [(float(e1[x]))*0.065*1e3 for x in range(len(e1))] # Scaling to domain and convertion to (g)
#########################################################################################
#readFile1=open('withoutThermalDiff-F-0.04.out', 'r')
#splitFile1=readFile1.read().split('\n')
#a2 = [row.split(';')[0] for row in splitFile1] # time
#a2 = [float(a2[x])/86400 for x in range(len(a2))] # Convertion to (Hrs)
#b2 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
#b2 = [float(b2[x])*86400 for x in range(len(b2))]# Convertion to (mm/day)
#c2 = [row.split(';')[2] for row in splitFile1] # Total water loss
#c2 = [(float(c2[0])-float(c2[x]))*0.065*1e3 for x in range(len(c2))] #Scaling to domain and convertion to (g)
#d2 = [row.split(';')[3] for row in splitFile1] # Salt loss
#d2 = [(float(d2[0])-float(d2[x]))*0.065*1e3 for x in range(len(d2))] #Scaling to domain and convertion to (g)
#e2 = [row.split(';')[4] for row in splitFile1] # Solid salt accumilation
#e2 = [(float(e2[x]))*0.065*1e3 for x in range(len(e2))] # Scaling to domain and convertion to (g)
########################################################################################
#readFile1=open('308.15corrHumidFlow.out', 'r')
readFile1=open('withoutThermalDiff-F-0.04.out', 'r') #Used 0.05 m/s results to represents 0.04 results (changes made in variable "b")
splitFile1=readFile1.read().split('\n')
a3 = [row.split(';')[0] for row in splitFile1] # time
a3 = [float(a3[x])/86400 for x in range(len(a3))] # Convertion to (Hrs)
b3 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
b3 = [float(b3[x])*86400 for x in range(len(b3))]# Convertion to (mm/day)
c3 = [row.split(';')[2] for row in splitFile1] # Total water loss
c3 = [(float(c3[0])-float(c3[x]))*0.065*1e3 for x in range(len(c3))] #Scaling to domain and convertion to (g)
d3 = [row.split(';')[3] for row in splitFile1] # Salt loss
d3 = [(float(d3[0])-float(d3[x]))*0.065*1e3 for x in range(len(d3))] #Scaling to domain and convertion to (g)
e3 = [row.split(';')[4] for row in splitFile1] # Solid salt accumilation
e3 = [(float(e3[x]))*0.065*1e3 for x in range(len(e3))] # Scaling to domain and convertion to (g)
########################################################################################
#readFile1=open('308.15-Hum-0.2.out', 'r')
readFile1=open('withoutThermalDiff-F-0.06.out', 'r')
splitFile1=readFile1.read().split('\n')
a4 = [row.split(';')[0] for row in splitFile1] # time
a4 = [float(a4[x])/86400 for x in range(len(a4))] # Convertion to (Hrs)
b4 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
b4 = [float(b4[x])*86400 for x in range(len(b4))]# Convertion to (mm/day)
c4 = [row.split(';')[2] for row in splitFile1] # Total water loss
c4 = [(float(c4[0])-float(c4[x]))*0.065*1e3 for x in range(len(c4))] #Scaling to domain and convertion to (g)
d4 = [row.split(';')[3] for row in splitFile1] # Salt loss
d4 = [(float(d4[0])-float(d4[x]))*0.065*1e3 for x in range(len(d4))] #Scaling to domain and convertion to (g)
e4 = [row.split(';')[4] for row in splitFile1] # Solid salt accumilation
e4 = [(float(e4[x]))*0.065*1e3 for x in range(len(e4))] # Scaling to domain and convertion to (g)
########################################################################################
#readFile1=open('evaporationData.out', 'r')
#splitFile1=readFile1.read().split('\n')
#a5 = [row.split(';')[0] for row in splitFile1] # time
#a5 = [float(a5[x])/86400 for x in range(len(a5))] # Convertion to (Hrs)
#b5 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
#b5 = [float(b5[x])*86400 for x in range(len(b5))]# Convertion to (mm/day)
#c5 = [row.split(';')[2] for row in splitFile1] # Total water loss
#c5 = [(float(c5[0])-float(c5[x]))*0.065*1e3 for x in range(len(c5))] #Scaling to domain and convertion to (g)
#d5 = [row.split(';')[3] for row in splitFile1] # Salt loss
#d5 = [(float(d5[0])-float(d5[x]))*0.065*1e3 for x in range(len(d5))] #Scaling to domain and convertion to (g)
#e5 = [row.split(';')[4] for row in splitFile1] # Solid salt accumilation
#e5 = [(float(e5[x]))*0.065*1e3 for x in range(len(e5))] # Scaling to domain and convertion to (g)
########################################################################################
#LEGEND LOCATION
#upper right	loc=1
#upper left	loc=2
#lower left	loc=3
#lower right	loc=4
###==========================EvapRate 0.5 M =========================================
fig3 = plt.figure()
ax3 = fig3.add_subplot(111)
#ax3.set_title("Cumulative Salt Precipitation", fontsize=20)    
ax3.set_xlabel('Time [day]', fontsize=30)
ax3.set_ylabel('Evap.Rate [mm/day]', fontsize=30)
ax3.set_ylim([0, 7.0])
ax3.set_xlim([0, 10])
ax3.tick_params(labelsize=30)
ax3.plot(time,evapRate,'k-', label='expData-0.5M',linewidth=3.0)
ax3.plot(a1,b1,'r-', label='0.02 [m/s]',linewidth=5.0)
#ax3.plot(a2,b2,'b-', label='308.15K-25%RH-0.027m/s-K=7e-11',linewidth=5.0)
ax3.plot(a3,b3,'b-', label='0.04 [m/s]',linewidth=5.0)
ax3.plot(a4,b4,'g-', label='0.06 [m/s]',linewidth=5.0)
#ax3.plot(a5,b5,'c-', label='308.15K-25%RH-0.05m/s-K=3e-10',linewidth=5.0)
leg = ax3.legend(loc=1,handlelength=2, borderpad=1.5, labelspacing=1.5, 
fontsize=20)
ax3.grid(True)
plt.show()
########################################################################################
#fig3 = plt.figure()
#ax3 = fig3.add_subplot(111)
##ax3.set_title("Cumulative Salt Precipitation", fontsize=20)    
#ax3.set_xlabel('Time [day]', fontsize=30)
#ax3.set_ylabel('Water loss [g]', fontsize=30)
##ax3.set_ylim([0, 7.0])
#ax3.set_xlim([0, 10])
#ax3.tick_params(labelsize=30)
#ax3.plot(time,wLoss,'ko', label='expData-0.5M',linewidth=3.0)
#ax3.plot(a1,c1,'r-', label='0.5 M-0.2RH',linewidth=5.0)
#ax3.plot(a2,c2,'b-', label='0.5 M',linewidth=5.0)
#leg = ax3.legend(loc=4,handlelength=2, borderpad=1.5, labelspacing=1.5, 
#fontsize=30)
#ax3.grid(True)
#plt.show()