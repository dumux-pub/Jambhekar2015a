#############################################################
#Configuration file for test_2cnistokes2p2cni
#############################################################

#############################################################
#General conditions: Problem, TimeManager, Vtk, Grid
#############################################################

#############################################################
[Problem]
#############################################################
#First part of the interface, where it is not coupled;
#set to zero or negative if not desired
RunUpDistanceX = 0.0 # 0.06, has to be larger than NoDarcyX !?
NoDarcyX = 0.0 # 0.05

#############################################################
[TimeManager]
#############################################################
#Initial and maximum time-step size
DtInitial = 1e-3
MaxTimeStepSize = 500

#Initialization time without coupling
#set to zero or negative if not desired
InitTime = 0

#Simulation end
TEnd = 1.2e5

#Define the length of an episode (for output)
EpisodeLength = 43200

#############################################################
[Vtk]
#############################################################
#Names for VTK output
NameFF = stokes2cni
NamePM = 2pncni
AddVelocity = 1 

#############################################################
[Grid]
#############################################################
UseInterfaceMeshCreator = true

#Name of the dgf file (grid)
File = grids/interfacedomain-PRE.dgf
Refinement = 0

#Number of elements in x-, y-, z-direction
CellsX = 30
CellsY = 51
CellsZ = 1
#Grading and refinement of the mesh
Grading = 1.13

#Extend of entire domain
XMin = 0.0
XMax = 0.01
YMin = 0.0
YMax = 0.04

#Vertical position of coupling interface
InterfacePos = 0.03

#############################################################
[Output]
#############################################################
#Frequency of restart file, flux and VTK output
FreqRestart = 1000      # how often restart files are written out 
FreqOutput = 20         # frequency of VTK output
FreqMassOutput = 2      # frequency of mass and evaporation rate output (Darcy)
FreqFluxOutput = 1000   # frequency of detailed flux output
FreqVaporFluxOutput = 2 # frequency of summarized flux output

#############################################################
[Stokes]
#############################################################
StabilizationAlpha = -1.0

#############################################################
[FreeFlow]
#############################################################
RefPressure = 1e5
RefTemperature = 305.15
RefMolefrac = 0.0062 # 20% Relative humidity for P_Sat = 3.1e3 [Pa] 
VxMax = 0.01
BeaversJosephSlipVel = 0.00134
ExponentMTC = 0.0 # 1./6., Mass transfer coefficient for S^MTC
UseBoundaryLayerModel = 0 # integer, 0 for no boundary layer model, 1 for Blasius, 2 for turbulent BL, 9 for constant thickness
BoundaryLayerOffset = 0.00 # For BL model like Blasius, determines a virtual run-up distance for the flow
ConstThickness = 0.0018 # for a constant BL thickness, use BL model 3
MassTransferModel = 0 # 0 for none, 1 for power law, 2 for Schluender model

SinusVelVar = 0.0 	# hourly velocity variation
SinusPVar = 0.0 	# diurnal pressure variation
SinusTVar = 0.0 	# diurnal temperature variation
SinusXVar = 0.0 	# diurnal variation of the vapor concentration (massfraction)

#############################################################
[PorousMedium]
#############################################################
RefPressurePM = 1e5
RefTemperaturePM = 305.15
InitialSw = 0.98
minPorosity = 0.1
CharPoreRadius = 1e-4
initialSalinity = 0.1895 # 4 Molal NaCl solution
solubilityLimit = 0.2641 # solubility limit for NaCl
PlotFluidProperties = false

#############################################################
[SpatialParams]
#############################################################
MaterialInterfaceX = 100.0
AlphaBJ1 = 1.0
AlphaBJ2 = 1.0 # added for simrun only not used in actual
AlphaBJ3 = 1.0 # added for simrun only not used in actual

# for homogeneous setups (0 for heterogeneous):
SoilType = 2
RegularizationThreshold = 1e-2

# GStat stuff, only required for SoilType=0
GenerateNewPermeability = true
GStatControlFileName = gstatControl_2D.txt
GStatInputFileName = gstatInput.txt
PermeabilityInputFileName = permeab.dat

### SoilType = 1 ### (Zurich coarse, MUSIS sand)
[SpatialParams.Coarse]
Permeability1 = 7.0e-10 #Kozeny-Carman; 1.387e-11 measured?
Porosity1 = 0.44
Swr1 = 0.005
Snr1 = 0.01
VgAlpha1 = 8.74e-4 
VgN1 = 3.35
PlotMaterialLaw1 = false
LambdaSolid1 = 5.26

Pentry1 = 1012
BCLambda1 = 3.277

### SoilType = 2 ### (Zurich fine)
[SpatialParams.Medium]
Permeability2 = 2.65e-10
Porosity2 = 0.37
Swr2 = 0.08
Snr2 = 0.01
VgAlpha2 = 6.02371e-4
VgN2 = 12.18
PlotMaterialLaw2 = false
LambdaSolid2 = 5.26

Pentry2 = 1012
BCLambda2 = 3.277

### SoilType = 3 ### (Manchester Sand)
[SpatialParams.Fine]
Permeability3 = 6.37e-12
Porosity3 = 0.37
Swr3 = 0.08
Snr3 = 0.01
VgAlpha3 = 6.02371e-4
VgN3 = 12.18
PlotMaterialLaw3 = true
LambdaSolid3 = 5.26

Pentry3 = 1012
BCLambda3 = 3.277

#############################################################
[Newton]
#############################################################
RelTolerance = 1e-5
TargetSteps = 8
MaxSteps = 12
WriteConvergence = false 
MaxTimeStepDivisions = 20 # should be in group Implicit

#############################################################
[LinearSolver]
#############################################################
ResidualReduction = 1e-9
Verbosity = 0
MaxIterations = 200
#GMResRestart = 100
#PreconditionerIterations = 2
