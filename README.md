Examples from Jambhekar et al. 2015
===================================

Jambhekar, V. A., Helmig, R., Schröder, N., Shokri, N. Transport in Porous Media, 2015
[Free-Flow--Porous-Media Coupling for Evaporation-Driven Transport and Precipitation of Salt in Soil](http://dx.doi.org/10.1007/s11242-015-0516-7)

For your convenience please find the [BibTex entry here](https://git.iws.uni-stuttgart.de/dumux-pub/Jambhekar2015a/raw/master/jambhekar2015.bib).

Installation
===================================

You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above. For the basic
dependencies see dune-project.org.

The easiest way is to use the `installJambhekar2015a.sh` in this folder.
You might want to look at it before execution [here](https://git.iws.uni-stuttgart.de/dumux-pub/Jambhekar2015a/raw/master/installJambhekar2015a.sh). Create a new folder containing the script and execute it.
You can copy the following to a terminal:
```bash
mkdir -p Jambhekar2015a && cd Jambhekar2015a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Jambhekar2015a/raw/master/installJambhekar2015a.sh
chmod +x installJambhekar2015a.sh && ./installJambhekar2015a.sh
```
It will install this module will take care of most dependencies.
For the basic dependencies see dune-project.org and at the end of this README.

How to run the examples
===================================

Once built please find the executables in the folder
`Jambhekar2015a/build-cmake/appl/salinization/FF-PMCoupling/NaCl/`
to run the example
```bash
$ cd Jambhekar2015a/build-cmake/appl/salinization/FF-PMCoupling/NaCl/
$ ./test_2cnistokes2pncni
```

Dependencies Info
===================================

When this module was created, the original module dumux-devel was using
the following list of DUNE modules and third-party libraries.
BEWARE: This does not necessarily mean that the applications in this
extracted module actually depend on all these components.

* dumux...................: version 2.6
* dune-common.............: version 2.3.1
* dune-geometry...........: version 2.3.1
* dune-grid...............: version 2.3.1
* dune-istl...............: version 2.3.1
* dune-localfunctions.....: version 2.3.1
* dune-multidomain........: version 2.0
* dune-multidomaingrid....: version 2.3.0
* dune-pdelab.............: version 2.0.0
* dune-typetree...........: version 2.3.1
* MPI.....................: no
* PETSc...................: no
* ParMETIS................: no
* SuperLU-DIST............: no
* SuperLU.................: yes (version 4.3 or newer)
* UG......................: yes (sequential)
* UMFPACK.................: no
* psurface................: no
