#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Checking out dependencies
### The branches are just supplied for readers of this script
### The actual revision are checked out below. This works
### independently of which branch was checked out.

### DUNE core modules
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-common.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-grid.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-istl.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-localfunctions.git

### DUNE multidomain
git clone -b releases/2.0 git://github.com/smuething/dune-multidomain.git
git clone -b releases/2.3 git://github.com/smuething/dune-multidomaingrid.git

### DUNE pdelab
git clone -b releases/2.0 https://gitlab.dune-project.org/pdelab/dune-pdelab.git
git clone -b releases/2.3 https://gitlab.dune-project.org/pdelab/dune-typetree.git

### UG
git clone https://gitlab.dune-project.org/ug/ug.git

### DuMuX
git clone -b releases/2.6 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git

### This module
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Jambhekar2015a.git

### checkout correct versions
cd dune-common && git checkout -q 2fe44e56be211cdf9c9336449cd89bca2ea58736 && cd ..
cd dune-geometry && git checkout -q 8e5075fc4c58e116d4a0f003a4d1f0482a3ab618 && cd ..
cd dune-grid && git checkout -q 9fe22cf9ecdba5fdbfd8e2c7a858a67b92626d3e && cd ..
cd dune-istl && git checkout -q e460af691168a6a9a7509aad1b13534e430f637c && cd ..
cd dune-localfunctions && git checkout -q eedfe2a7639be4a7e6dc457fc5181454088aebe0 && cd ..

cd dune-multidomain && git checkout -q b38dcc116153d4e39181f984383cefa7afd8139f && cd ..
cd dune-multidomaingrid && git checkout -q e324ae796ec9607727869bd35e14e005f3738aa0 && cd ..

cd dune-pdelab && git checkout -q f7a7ff4a23e6f43967b006318480ffbf894ff63f && cd ..
cd dune-typetree && git checkout -q ecffa10c59fa61a0071e7c788899464b0268719f && cd ..

cd ug && git checkout -q v3.12.1 && cd ..

cd dumux && git checkout -q 12dcfef8b71dbc8be815df7e8148d9eba7e45c74 && cd ..

### apply necessary patches
cd dune-grid && patch -p1 < ../Jambhekar2015a/patches/dune-grid.patch && cd ..

### build UG
cd ug
autoreconf -is
OPTIM_FLAGS="-O3 -DNDEBUG -march=native -finline-functions -funroll-loops"
CFLAGS="$OPTIM_FLAGS"
CXXFLAGS="$OPTIM_FLAGS -std=c++0x -fno-strict-aliasing"
OPTS="--enable-dune --prefix=$PWD"
./configure CFLAGS="$CFLAGS" CXXFLAGS="$CXXFLAGS" $OPTS
make
make install
cd ..

### run dunecontrol
pwd > temp.txt
sed -i 's/\//\\\//g' temp.txt
EXTPATH=`cat temp.txt`
/usr/bin/rm temp.txt
sed "s/DUG_ROOT=.*/DUG_ROOT=$EXTPATH\/ug/" Jambhekar2015a/dunecontrol.opts > Jambhekar2015a/dunecontrol_used.opts
./dune-common/bin/dunecontrol --opts=Jambhekar2015a/dunecontrol_used.opts all
