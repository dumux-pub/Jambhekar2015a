dnl -*- autoconf -*-
# Macros needed to find Jambhekar2015a and dependent libraries.  They are called by
# the macros in ${top_src_dir}/dependencies.m4, which is generated by
# "dunecontrol autogen"

# Additional checks needed to build Jambhekar2015a
# This macro should be invoked by every module which depends on Jambhekar2015a, as
# well as by Jambhekar2015a itself
AC_DEFUN([JAMBHEKAR_2015A_TIPM_CHECKS])

# Additional checks needed to find Jambhekar2015a
# This macro should be invoked by every module which depends on Jambhekar2015a, but
# not by Jambhekar2015a itself
AC_DEFUN([JAMBHEKAR_2015A_TIPM_CHECK_MODULE],
[
  DUNE_CHECK_MODULES([Jambhekar2015a],[Jambhekar2015a/Jambhekar2015a.hh])
])
