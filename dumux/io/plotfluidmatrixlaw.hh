/*****************************************************************************
 *   Copyright (C) 2011 by Markus Wolff                                      *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Interface for plotting of fluid-matrix-interaction laws
 *
 */
#ifndef DUMUX_PLOT_FLUID_MATRIX_LAW_HH
#define DUMUX_PLOT_FLUID_MATRIX_LAW_HH

#include "gnuplotinterface.hh"

namespace Dumux
{
/*!
 * \ingroup fluidmatrixinteractionslaws
 *
 *\brief Interface for plotting of fluid-matrix-interaction laws
 *
 */
template<class MaterialLaw>
class PlotFluidMatrixLaw
{
    typedef typename MaterialLaw::Scalar Scalar;
    typedef typename MaterialLaw::Params MaterialLawParams;
public:
    /*!
     * \brief docme
     *
     *  \param storeFiles docme
     *  \param numIntervals docme
     */
    PlotFluidMatrixLaw(bool storeFiles = false,
    				   int numIntervals = 1000) :
        storeFiles_(storeFiles), numIntervals_(numIntervals)
    {
        plot_.setStyle(GnuplotInterface::lines);
    }
    /*!
     * \brief docme
     *
     *  \param storeFiles docme
     */
    void setStoreFlag(bool storeFiles)
    {
        storeFiles_ = storeFiles;
    }

    /*!
     * \brief Plot the capillary pressure-saturation curve
     *
     * \param params docme
     * \param lowerSat docme
     * \param upperSat docme
     * \param plotName docme
     */
    void plotpC(const MaterialLawParams &params,
    			Scalar lowerSat = 0.0,
    			Scalar upperSat = 1.0,
    			const std::string plotName = "pc_Sw")
    {
        std::vector<Scalar> sat(numIntervals_+1);
        std::vector<Scalar> pc(numIntervals_+1);
        Scalar sInterval = (upperSat - lowerSat);

        for (int i = 0; i <= numIntervals_; i++)
        {
            sat[i] = lowerSat + sInterval * double(i) / double(numIntervals_);
            pc[i] = MaterialLaw::pc(params, sat[i]);
        }

        plot_.setXRange(lowerSat - epsSat_, upperSat + epsSat_);
        plot_.setYRange(std::min(pc[numIntervals_] - epsPc_, 0.0), pc[0] + epsPc_);
        plot_.setXlabel("wetting phase saturation [-]");
        plot_.setYlabel("p_c [Pa]");
        plot_.plot(sat, pc, plotName, 1, storeFiles_);
    }

    /*!
     * \brief Plot the saturation-capillary pressure curve.
     *
     *  \param params docme
     *  \param lowerpC docme
     *  \param upperpC docme
     *  \param plotName docme
     */
    void plotSw(const MaterialLawParams &params,
    			Scalar lowerpC = 0.0,
    			Scalar upperpC = 5000,
    			const std::string plotName = "Sw_pc")
    {
        std::vector<Scalar> sat(numIntervals_+1);
        std::vector<Scalar> pc(numIntervals_+1);
        Scalar pCInterval = (upperpC - lowerpC);

        for (int i = 0; i <= numIntervals_; i++)
        {
            pc[i] = lowerpC + pCInterval * double(i) / double(numIntervals_);
            sat[i] = MaterialLaw::Sw(params, pc[i]);
        }

        plot_.setXRange(lowerpC - epsPc_, upperpC + epsPc_);
        plot_.setYRange(sat[numIntervals_] - epsSat_, sat[0] + epsSat_);
        plot_.setXlabel("p_c [Pa]");
        plot_.setYlabel("S_w [-]");
        plot_.plot(pc, sat, plotName, 2, storeFiles_);
    }

    /*!
     * \brief Plot the partial derivative of the capillary
     *        pressure
     *
     *  \param params docme
     *  \param lowerSat docme
     *  \param upperSat docme
     *  \param plotName docme
     */
    void plotdpC_dSw(const MaterialLawParams &params,
    				 Scalar lowerSat = 0.0,
    				 Scalar upperSat = 1.0,
    				 const std::string plotName = "dpc_dSw")
    {
        std::vector<Scalar> sat(numIntervals_ + 1);
        std::vector<Scalar> dpcdSw(numIntervals_ + 1);
        Scalar sInterval = (upperSat - lowerSat);

        Scalar dpcdSwMin = 1e100;
        Scalar dpcdSwMax = -1e100;

        for (int i = 0; i <= numIntervals_; i++)
        {
            sat[i] = lowerSat + sInterval * double(i) / double(numIntervals_);
            dpcdSw[i] = MaterialLaw::dpC_dSw(params, sat[i]);
            dpcdSwMin = std::min(dpcdSwMin, dpcdSw[i]);
            dpcdSwMax = std::max(dpcdSwMax, dpcdSw[i]);
        }

        plot_.setXRange(lowerSat - epsSat_, upperSat + epsSat_);
        Scalar epsdpcdSw = std::abs(dpcdSwMax-dpcdSwMin)/20;
        plot_.setYRange(dpcdSwMin - epsdpcdSw, dpcdSwMax + epsdpcdSw);
        plot_.setXlabel("wetting phase saturation [-]");
        plot_.setYlabel("dp_c/dSw [Pa]");
        plot_.plot(sat, dpcdSw, plotName, 3, storeFiles_);
    }

    /*!
     * \brief Plot the partial derivative of the effective
     *        saturation
     *
     *  \param params docme
     *  \param lowerpC docme
     *  \param upperpC docme
     *  \param plotName docme
     */
    void plotdSw_dpC(const MaterialLawParams &params,
    				 Scalar lowerpC = 0.0,
    				 Scalar upperpC = 5000,
    				 const std::string plotName = "dSw_dpC")
    {
        std::vector<Scalar> dSwdpC(numIntervals_+1);
        std::vector<Scalar> pc(numIntervals_+1);
        Scalar pCInterval = (upperpC - lowerpC);

        Scalar dSwdpCMin = 1e100;
        Scalar dSwdpCMax = -1e100;

        for (int i = 0; i <= numIntervals_; i++)
        {
            pc[i] = lowerpC + pCInterval * double(i) / double(numIntervals_);
            dSwdpC[i] = MaterialLaw::dSw_dpC(params, pc[i]);
            dSwdpCMin = std::min(dSwdpCMin, dSwdpC[i]);
            dSwdpCMax = std::max(dSwdpCMax, dSwdpC[i]);
        }

        plot_.setXRange(lowerpC - epsPc_, upperpC + epsPc_);
        Scalar epsdSwdpC = std::abs(dSwdpCMax-dSwdpCMin)/20;
        plot_.setYRange(dSwdpCMin - epsdSwdpC, dSwdpCMax + epsdSwdpC);
        plot_.setXlabel("p_c [Pa]");
        plot_.setYlabel("dS_w/dp_c [1/Pa]");
        plot_.plot(pc, dSwdpC, plotName, 4, storeFiles_);
    }

    /*!
     * \brief Plot the relative permeability of the wetting phase.
     *
     *  \param params docme
     *  \param lowerSat docme
     *  \param upperSat docme
     *  \param plotName docme
     */
    void plotkrw(const MaterialLawParams &params,
    			 Scalar lowerSat = 0.0,
    			 Scalar upperSat = 1.0,
    			 const std::string plotName = "kr_w")
    {
        std::vector<Scalar> sat(numIntervals_ + 1);
        std::vector<Scalar> krw(numIntervals_ + 1);
        Scalar sInterval = (upperSat - lowerSat);

        for (int i = 0; i <= numIntervals_; i++)
        {
            sat[i] = lowerSat + sInterval * double(i) / double(numIntervals_);
            krw[i] = MaterialLaw::krw(params, sat[i]);
        }

        plot_.setXRange(lowerSat - epsSat_, upperSat + epsSat_);
        plot_.setYRange(std::min(krw[0] - epsKr_, 0.0), std::max(krw[numIntervals_] + epsKr_, 1.0));
        plot_.setXlabel("wetting phase saturation [-]");
        plot_.setYlabel("k_r_w [-]");
        plot_.plot(sat, krw, plotName, 5, storeFiles_);
    }
    ;

    /*!
     * \brief Plot the tensorial relative permeability of the wetting phase.
     *
     *  \param params docme
     *  \param lowerSat docme
     *  \param upperSat docme
     */
    void plotKrw(const MaterialLawParams &params,
    			 Scalar lowerSat = 0.0,
    			 Scalar upperSat = 1.0)
    {
        int dim = MaterialLaw::Krw(params, 0).N();
        std::vector<Scalar> sat(numIntervals_ + 1);
        std::vector < std::vector<Scalar> > krwvec(numIntervals_ + 1,
                std::vector<Scalar>(dim * dim));
        std::vector<std::string> krwlegend(dim * dim);
        Scalar sInterval = (upperSat - lowerSat);

        Scalar krwMin = 1;
        Scalar krwMax = 0;

        for (int i = 0; i <= numIntervals_; i++)
        {
            sat[i] = lowerSat + sInterval * double(i) / double(numIntervals_);
            typename MaterialLaw::Matrix krw = MaterialLaw::Krw(params, sat[i]);
            switch (dim)
            {
            case 2:
            {
                krwvec[i][0] = krw[0][0];
                krwvec[i][1] = krw[0][1];
                krwvec[i][2] = krw[1][0];
                krwvec[i][3] = krw[1][1];
                krwMin=std::min(krw[0][0], std::min(krw[0][1], std::min(krw[1][0], std::min(krw[1][1],krwMin))));
                krwMax=std::max(krw[0][0], std::max(krw[0][1], std::max(krw[1][0], std::max(krw[1][1],krwMax))));
                krwlegend[0] = "k_r_w_xx";
                krwlegend[1] = "k_r_w_xy";
                krwlegend[2] = "k_r_w_yx";
                krwlegend[3] = "k_r_w_yy";
                break;
            }
            case 3:
            {
                krwvec[i][0] = krw[0][0];
                krwvec[i][1] = krw[0][1];
                krwvec[i][2] = krw[0][2];
                krwvec[i][3] = krw[1][0];
                krwvec[i][4] = krw[1][1];
                krwvec[i][5] = krw[1][2];
                krwvec[i][6] = krw[2][0];
                krwvec[i][7] = krw[2][1];
                krwvec[i][8] = krw[2][2];

                for (int j = 0; j<9;j++)
                {
                    krwMin=std::min(krwMin, krwvec[i][j]);
                    krwMax=std::max(krwMax, krwvec[i][j]);
                }
                krwlegend[0] = "k_r_w_xx";
                krwlegend[1] = "k_r_w_xy";
                krwlegend[2] = "k_r_w_xz";
                krwlegend[3] = "k_r_w_yx";
                krwlegend[4] = "k_r_w_yy";
                krwlegend[5] = "k_r_w_yz";
                krwlegend[6] = "k_r_w_zx";
                krwlegend[7] = "k_r_w_zy";
                krwlegend[8] = "k_r_w_zz";

                break;
            }
            default:
                break;
            }
        }

        plot_.setXRange(lowerSat, upperSat);
        plot_.setYRange(krwMin - epsKr_, krwMax + epsKr_);
        plot_.setXlabel("wetting phase saturation [-]");
        plot_.setYlabel("k_r [-]");
        plot_.plot(sat, krwvec, krwlegend, 5, storeFiles_);
    }
    ;

    /*!
     * \brief Plot the derivative of the relative permeability for the
     *        wetting phase with regard to the wetting saturation
     */
//    void plotdkrw_dSw(const MaterialLawParams &params, Scalar lowerSat = 0.0,
//            Scalar upperSat = 1.0)
//    {
//        std::vector<Scalar> sat(numIntervals_ + 1);
//        std::vector<Scalar> dkrwdSw(numIntervals_ + 1);
//        Scalar sInterval = (upperSat - lowerSat);
//
//        for (int i = 0; i <= numIntervals_; i++)
//        {
//            sat[i] = lowerSat + sInterval * double(i) / double(numIntervals_);
//            dkrwdSw[i] = MaterialLaw::dkrw_dSw(params, sat[i]);
//        }
//
//        plot_.setXlabel("wetting phase saturation [-]");
//        plot_.setYlabel("dk_r/dSw [-]");
//        plot_.plot(sat, dkrwdSw, "dkrw_dSw", 6, storeFiles_);
//    }
//    ;

    /*!
     * \brief Plot the relative permeability for the non-wetting phase
     *
     *  \param params docme
     *  \param lowerSat docme
     *  \param upperSat docme
     *  \param plotName
     */
    void plotkrn(const MaterialLawParams &params,
    			 Scalar lowerSat = 0.0,
    			 Scalar upperSat = 1.0,
    			 const std::string plotName = "kr_n")
    {
        std::vector<Scalar> sat(numIntervals_ + 1);
        std::vector<Scalar> krn(numIntervals_ + 1);
        Scalar sInterval = (upperSat - lowerSat);

        for (int i = 0; i <= numIntervals_; i++)
        {
            sat[i] = lowerSat + sInterval * double(i) / double(numIntervals_);
            krn[i] = MaterialLaw::krn(params, sat[i]);
        }

        plot_.setXRange(lowerSat - epsSat_, upperSat + epsSat_);
        plot_.setYRange(std::min(krn[numIntervals_] - epsKr_, 0.0), std::max(krn[0] + epsKr_, 1.0));
        plot_.setXlabel("wetting phase saturation [-]");
        plot_.setYlabel("k_r [-]");
        plot_.plot(sat, krn, plotName, 5, storeFiles_);
    }

    /*!
     * \brief Plot the derivative of the relative permeability for the
     *        non-wetting phase in regard to the wetting saturation.
     */
//    void plotdkrn_dSw(const MaterialLawParams &params, Scalar lowerSat = 0.0,
//            Scalar upperSat = 1.0)
//    {
//        std::vector<Scalar> sat(numIntervals_ + 1);
//        std::vector<Scalar> dkrndSw(numIntervals_ + 1);
//        Scalar sInterval = (upperSat - lowerSat);
//
//        for (int i = 0; i <= numIntervals_; i++)
//        {
//            sat[i] = lowerSat + sInterval * double(i) / double(numIntervals_);
//            dkrndSw[i] = MaterialLaw::dkrn_dSw(params, sat[i]);
//        }
//
//        plot_.setXlabel("wetting phase saturation [-]");
//        plot_.setYlabel("dk_r/dSw [-]");
//        plot_.plot(sat, dkrndSw, "dkrn_dSw", 6, storeFiles_);
//    }
//    ;

    /*!
     * \brief Plot the tensorial relative permeability of the non_wetting phase.
     *
     *  \param params docme
     *  \param lowerSat docme
     *  \param upperSat docme
     */
    void plotKrn(const MaterialLawParams &params,
    			 Scalar lowerSat = 0.0,
    			 Scalar upperSat = 1.0)
    {
        int dim = MaterialLaw::Krw(params, 0).N();
        std::vector<Scalar> sat(numIntervals_ + 1);
        std::vector < std::vector<Scalar> > krnvec(numIntervals_ + 1,
                std::vector<Scalar>(dim * dim));
        std::vector < std::string > krnlegend(dim * dim);
        Scalar sInterval = (upperSat - lowerSat);

        Scalar krnMin = 1;
        Scalar krnMax = 0;

        for (int i = 0; i <= numIntervals_; i++)
        {
            sat[i] = lowerSat + sInterval * double(i) / double(numIntervals_);
            typename MaterialLaw::Matrix krn = MaterialLaw::Krn(params, sat[i]);
            switch (dim)
            {
            case 2:
            {
                krnvec[i][0] = krn[0][0];
                krnvec[i][1] = krn[0][1];
                krnvec[i][2] = krn[1][0];
                krnvec[i][3] = krn[1][1];
                krnMin=std::min(krn[0][0], std::min(krn[0][1], std::min(krn[1][0], std::min(krn[1][1],krnMin))));
                krnMax=std::max(krn[0][0], std::max(krn[0][1], std::max(krn[1][0], std::max(krn[1][1],krnMax))));
                krnlegend[0] = "k_r_n_xx";
                krnlegend[1] = "k_r_n_xy";
                krnlegend[2] = "k_r_n_yx";
                krnlegend[3] = "k_r_n_yy";
                break;
            }
            case 3:
            {
                krnvec[i][0] = krn[0][0];
                krnvec[i][1] = krn[0][1];
                krnvec[i][2] = krn[0][2];
                krnvec[i][3] = krn[1][0];
                krnvec[i][4] = krn[1][1];
                krnvec[i][5] = krn[1][2];
                krnvec[i][6] = krn[2][0];
                krnvec[i][7] = krn[2][1];
                krnvec[i][8] = krn[2][2];

                for (int j = 0; j<9;j++)
                {
                    krnMin=std::min(krnMin, krnvec[i][j]);
                    krnMax=std::max(krnMax, krnvec[i][j]);
                }
                krnlegend[0] = "k_r_n_xx";
                krnlegend[1] = "k_r_n_xy";
                krnlegend[2] = "k_r_n_xz";
                krnlegend[3] = "k_r_n_yx";
                krnlegend[4] = "k_r_n_yy";
                krnlegend[5] = "k_r_n_yz";
                krnlegend[6] = "k_r_n_zx";
                krnlegend[7] = "k_r_n_zy";
                krnlegend[8] = "k_r_n_zz";

                break;
            }
            default:
                break;
            }
        }

        plot_.setXRange(lowerSat, upperSat);
        plot_.setYRange(krnMin - epsKr_, krnMax + epsKr_);
        plot_.setXlabel("wetting phase saturation [-]");
        plot_.setYlabel("k_r [-]");
        plot_.plot(sat, krnvec, krnlegend, 5, storeFiles_);
    }
    ;

private:
    GnuplotInterface plot_;
    bool storeFiles_;
    int numIntervals_;
    static constexpr Scalar epsSat_ = 0.01;
    static constexpr Scalar epsKr_ = 0.05;
    static constexpr Scalar epsPc_ = 50;
};
}

#endif
