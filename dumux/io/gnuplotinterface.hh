// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief docme
 */
#ifndef GNUPLOTINTERFACE_HH
#define GNUPLOTINTERFACE_HH

namespace Dumux
{

class GnuplotInterface
{
    typedef std::vector<std::string> StringVector;

public:

    enum PlotStyle
    {
        lines, points, linesPoints, impulses, dots
    };

    //1D plot
    /*!
     * \brief docme
     *
     *  \param title The name of the output file
     *  \param plottingWindowNumber docme
     *  \param temporary docme
     */
    void plot(const std::vector<double>&,
    		  const std::string& title = "data",
    		  int plottingWindowNumber = 1,
    		  bool temporary = true);

    //2D plot
    /*!
     * \brief docme
     *
     *  \param title The name of the output file
     *  \param plottingWindowNumber docme
     *  \param temporary docme
     */
    void plot(const std::vector<double>&,
    		  const std::vector<double>&,
    		  const std::string& title = "data",
    		  int plottingWindowNumber = 1,
    		  bool temporary = true);

    /*!
     * \brief docme
     *
     *  \param plottingWindowNumber docme
     *  \param temporary docme
     */
    void plot(const std::vector<double>&,
              const std::vector<std::vector<double> >&,
              const std::vector<std::string>&,
              int plottingWindowNumber = 1,
              bool temporary = true);

    //plot data file 2D
    /*!
     * \brief docme
     *
     * \param plottingWindowNumber docme
     */
    void plotDataFile(const std::string&,
    				  const std::string&,
    				  int plottingWindowNumber);

    /*!
    * \brief docme
    *
    *  \param numPlots docme
    *  \param plottingWindowNumber docme
    */
    void plotDataFile(const std::string&,
    				  const std::vector<std::string>&,
    				  int numPlots,
    				  int plottingWindowNumber);

    /*!
     * \brief docme
     *
     *  \param label docme
     */
    void setYlabel(const std::string& label)
    {
        if (haveGnuplot_)
        {
            std::string plotCommand = "set ylabel \"" + label + "\"";
            executeGnuplot(plotCommand.c_str());
        }
    }

    /*!
     * \brief docme
     *
     *  \param label docme
     */
    void setXlabel(const std::string& label)
    {
        if (haveGnuplot_)
        {
            std::string plotCommand = "set xlabel \"" + label + "\"";
            executeGnuplot(plotCommand.c_str());
        }
    }

    /*!
     * \brief docme
     *
     *  \param lowerEnd docme
     *  \param upperEnd docme
     */
    void setXRange(double lowerEnd,
    			   double upperEnd)
    {
        if (haveGnuplot_)
        {
            std::string plotCommand = "set xrange [" + numberToString(lowerEnd) + ":" + numberToString(upperEnd)
                    + "]";
            executeGnuplot(plotCommand.c_str());
        }
    }

    /*!
     * \brief docme
     *
     *  \param lowerEnd docme
     *  \param upperEnd docme
     */
    void setYRange(double lowerEnd,
    			   double upperEnd)
    {
        if (haveGnuplot_)
        {
            std::string plotCommand = "set yrange [" + numberToString(lowerEnd) + ":" + numberToString(upperEnd)
                    + "]";
            executeGnuplot(plotCommand.c_str());
        }
    }

    /*!
     * \brief docme
     *
     *  \param style docme
     */
    void setStyle(const PlotStyle& style)
    {
        switch (style)
        {
        case lines:
            plotStyle_ = "lines";
            break;
        case points:
            plotStyle_ = "points";
            break;
        case linesPoints:
            plotStyle_ = "linespoints";
            break;
        case impulses:
            plotStyle_ = "impulses";
            break;
        case dots:
            plotStyle_ = "dots";
            break;
        default:
            assert(!"Unknown plot style");
        }
    }

    /*!
     * \brief docme
     *
     *  \param newPath docme
     */
    void setPath(const std::string& newPath)
    {
        gnuplotPath_ = newPath;
    }

private:
    //give plot command to gnuplot
    void executeGnuplot(const std::string& plotCommand) const
    {
        fputs((plotCommand + "\n").c_str(), pipe_);
        fflush(pipe_);
    }

    //Checks if gnuplot is available
    bool haveGnuplot() const
    {
        std::fstream stream;
        stream.open(gnuplotPath_.c_str(), std::ios::in);
        if (stream.is_open())
        {
            stream.close();
            return true;
        }
        stream.close();
        return false;
    }
    ///Convert number to string
    template<class T> std::string numberToString(const T number) const
    {
        std::ostringstream stream;
        stream << number;
        return stream.str();
    }

public:
    GnuplotInterface() :
        pipe_(0), plotStyle_("points"), oldWindow_(0), lastWindowNum_(1),
                fileNames_(0), gnuplotPath_("/usr/bin/gnuplot"), terminalType_(
                        "wxt")
    {
        if (haveGnuplot())
        {
            haveGnuplot_ = true;

            //            pipe_ = popen(gnuplotPath_.c_str(), "w"); // "w" - writing
            pipe_ = popen((gnuplotPath_ + " -persist").c_str(), "w"); // "w" - writing

            if (pipe_ == 0)
            {
                std::cout << "Couldn't open connection to gnuplot!"
                        << "\nGnuplot visualization deactivated!" << std::endl;
                haveGnuplot_ = false;
            }
        }
        else
        {
            std::cout << "Gnuplot: Unknown path!"
                    << "\nGnuplot visualization deactivated!" << std::endl;
            haveGnuplot_ = false;
        }
    }

    ~GnuplotInterface()
    {
        if (haveGnuplot_)
        {
            for (unsigned int i = 0; i < fileNames_.size(); i++)
            {
                for (unsigned int j = 0; j < fileNames_[i].size(); j++)
                {
                    std::remove(fileNames_[i][j].c_str());
                }
            }

            if (pclose(pipe_) == -1)
                assert("Could not close pipe to Gnuplot!");
        }
    }
private:
    bool haveGnuplot_;
    std::FILE * pipe_;
    std::string plotStyle_;
    std::vector<bool> oldWindow_;
    int lastWindowNum_;
    std::vector<StringVector> fileNames_;
    std::string gnuplotPath_;
    std::string terminalType_;
};

/*!
 * \brief docme
 *
 *  \param x docme
 *  \param title docme
 *  \param plottingWindowNumber docme
 *  \param temporary docme
 */
void GnuplotInterface::plot(const std::vector<double>& x,
							const std::string &title,
							int plottingWindowNumber,
							bool temporary)
{
    if (haveGnuplot_)
    {
        //write data to file
        std::string fileName = title + "_" + numberToString(plottingWindowNumber);
        std::ofstream file;
        file.open(fileName);
        const int size = x.size();
        for (int i = 0; i != size; i++)
            file << x[i] << std::endl;
        file.close();
        if (temporary)
        {
            if (fileNames_.size() < plottingWindowNumber)
            {
                StringVector sv(0);
                while (fileNames_.size() < plottingWindowNumber)
                    fileNames_.push_back(sv);
            }
            fileNames_[plottingWindowNumber].push_back(fileName);
        }

        if (oldWindow_.size() < plottingWindowNumber)
        {
            bool num = false;
            while (oldWindow_.size() < plottingWindowNumber)
                oldWindow_.push_back(num);
        }

        if (plottingWindowNumber != lastWindowNum_)
        {
            lastWindowNum_ = plottingWindowNumber;
            const std::string plotCommand = "set term wxt " + numberToString(
                    plottingWindowNumber);
            executeGnuplot(plotCommand.c_str());
        }

        //plot from file
        plotDataFile(fileName, title, plottingWindowNumber);
    }
}

/*!
 * \brief docme
 *
 *  \param x docme
 *  \param y docme
 *  \param title docme
 *  \param plottingWindowNumber docme
 *  \param temporary docme
 */
void GnuplotInterface::plot(const std::vector<double>& x,
							const std::vector<double>& y,
							const std::string &title,
							int plottingWindowNumber,
							bool temporary)
{
    if (haveGnuplot_)
    {
        assert(x.size() == y.size());

        //write data to file
        std::string fileName = title + "_" + numberToString(plottingWindowNumber);
        std::ofstream file;
        file.open(fileName);
        for (unsigned int i = 0; i < x.size(); i++)
        {
            file << x[i] << " " << y[i] << std::endl;
        }
        file.close();
        if (temporary)
        {
            if (fileNames_.size() < plottingWindowNumber)
            {
                StringVector sv(0);
                while (fileNames_.size() < plottingWindowNumber)
                    fileNames_.push_back(sv);
            }
            fileNames_[plottingWindowNumber - 1].push_back(fileName);
        }

        if (oldWindow_.size() < plottingWindowNumber)
        {
            bool num = false;
            while (oldWindow_.size() < plottingWindowNumber)
                oldWindow_.push_back(num);
        }

        if (plottingWindowNumber != lastWindowNum_)
        {
            lastWindowNum_ = plottingWindowNumber;
            const std::string plotCommand = "set term wxt " + numberToString(
                    plottingWindowNumber);
            executeGnuplot(plotCommand.c_str());
        }

        //plot from file
        plotDataFile(fileName, title, plottingWindowNumber);
    }
}

/*!
 * \brief docme
 *
 *  \param x docme
 *  \param y docme
 *  \param title docme
 *  \param plottingWindowNumber docme
 *  \param temporary docme
 */
void GnuplotInterface::plot(const std::vector<double>& x,
							const std::vector<std::vector<double> >& y,
							const std::vector<std::string> &title,
							int plottingWindowNumber,
							bool temporary)
{
    if (haveGnuplot_)
    {
        assert(x.size() == y.size());

        //write data to file
        std::string fileName = title[0] + "_" + numberToString(
                plottingWindowNumber);
        std::ofstream file;
        file.open(fileName);
        const unsigned int size = x.size();
        for (unsigned int i = 0; i < size; i++)
        {
            file << x[i] << " ";
            for (unsigned int j = 0; j != y[i].size(); j++)
                file << y[i][j] << " ";
            file << std::endl;
        }
        file.close();
        if (temporary)
        {
            if (fileNames_.size() < plottingWindowNumber)
            {
                StringVector sv(0);
                while (fileNames_.size() < plottingWindowNumber)
                    fileNames_.push_back(sv);
            }
            fileNames_[plottingWindowNumber - 1].push_back(fileName);
        }

        if (oldWindow_.size() < plottingWindowNumber)
        {
            bool num = false;
            while (oldWindow_.size() < plottingWindowNumber)
                oldWindow_.push_back(num);
        }

        if (plottingWindowNumber != lastWindowNum_)
        {
            lastWindowNum_ = plottingWindowNumber;
            const std::string plotCommand = "set term wxt " + numberToString(
                    plottingWindowNumber);
            executeGnuplot(plotCommand.c_str());
        }

        //plot from file
        plotDataFile(fileName, title, y[0].size(), plottingWindowNumber);
    }
}

/*!
 * \brief docme
 *
 *  \param fileName docme
 *  \param title docme
 *  \param plottingWindowNumber docme
 */
void GnuplotInterface::plotDataFile(const std::string& fileName,
        							const std::string& title,
        							int plottingWindowNumber)
{
    if (haveGnuplot_)
    {
        std::string plotCommand;

        if (oldWindow_[plottingWindowNumber - 1])
        {
            plotCommand = "replot \"" + fileName + "\" title \"" + title
                    + "\" with " + plotStyle_;
        }
        else
        {
            plotCommand = "plot \"" + fileName + "\" title \"" + title
                    + "\" with " + plotStyle_;
            oldWindow_[plottingWindowNumber - 1] = true;
        }

        executeGnuplot(plotCommand.c_str());
    }
}

/*!
 * \brief docme
 *
 *  \param fileName docme
 *  \param title docme
 *  \param numPlots docme
 *  \param plottingWindowNumber docme
 */
void GnuplotInterface::plotDataFile(const std::string& fileName,
									const std::vector<std::string>& title, int numPlots,
									int plottingWindowNumber)
{
    if (haveGnuplot_)
    {
        std::string plotCommand;

        if (oldWindow_[plottingWindowNumber - 1])
        {
            plotCommand = "replot \"" + fileName + "\" using " + "1:2"
                    + " with " + plotStyle_ + " title \"" + title[0] + "\"";
            for (int i = 3; i <= numPlots + 1; i++)
            {
                char num[10];
                sprintf(num, "%d", i);
                plotCommand += ", \"" + fileName + "\" using " + "1:" + num
                        + " with " + plotStyle_ + " title \"" + title[i - 2]
                        + "\"";
            }
        }
        else
        {
            plotCommand = "plot \"" + fileName + "\" using " + "1:2" + " with "
                    + plotStyle_ + " title \"" + title[0] + "\"";
            for (int i = 3; i <= numPlots + 1; i++)
            {
                char num[10];
                sprintf(num, "%d", i);
                plotCommand += ", \"" + fileName + "\" using " + "1:" + num
                        + " with " + plotStyle_ + " title \"" + title[i - 2]
                        + "\"";
            }
            oldWindow_[plottingWindowNumber - 1] = true;
        }

        executeGnuplot(plotCommand.c_str());
    }
}
} //end of namespace
#endif
