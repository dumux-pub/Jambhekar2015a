/****************************************************************************
 *   Copyright (C) 2010 by Klaus Mosthaf                                     *
 *   Copyright (C) 2008-2010 by Andreas Lauser                               *
 *   Copyright (C) 2008-2010 by Bernd Flemisch                               *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Reference implementation of a newton controller for coupled 2cStokes - 2p2c problems.
 */
#ifndef DUMUX_TWOCSTOKES_2PNCMIN_NEWTON_CONTROLLER_HH
#define DUMUX_TWOCSTOKES_2PNCMIN_NEWTON_CONTROLLER_HH

#include <dumux/multidomain/common/multidomainnewtoncontroller.hh>

/*!
 * \file
 */
namespace Dumux
{

/*!
 * \brief Implementation of a newton controller for 2cstokes2pncMin problems.
 */
template <class TypeTag>
class TwoCStokesTwoPNCMinNewtonController : public MultiDomainNewtonController<TypeTag>
{
    typedef MultiDomainNewtonController<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

public:
    TwoCStokesTwoPNCMinNewtonController(const Problem &problem)
        : ParentType(problem)
	{  }

	//! \copydoc Dumux::NewtonController::newtonEndStep()
	void newtonEndStep(SolutionVector &uCurrentIter, SolutionVector &uLastIter)
	{
		ParentType::newtonEndStep(uCurrentIter, uLastIter);

		this->model_().sdModel2().updateStaticData(this->model_().sdModel2().curSol(),
												   this->model_().sdModel2().prevSol());
	}
};

} // namespace Dumux


#endif
