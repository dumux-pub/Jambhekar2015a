
install(FILES
        coupledproperties.hh
        multidomainboxlocaloperator.hh
        subdomainpropertydefaults.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/modelcoupling/common)
