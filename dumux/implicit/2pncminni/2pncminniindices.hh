// $Id: 2p2cproperties.hh 3784 2010-06-24 13:43:57Z bernd $
/*****************************************************************************
 *   Copyright (C) 2008 by Klaus Mosthaf, Andreas Lauser, Bernd Flemisch     *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 *
 * \brief Defines the indices required for the 2pncMin BOX model.
 */
#ifndef DUMUX_2PNCMINNI_INDICES_HH
#define DUMUX_2PNCMINNI_INDICES_HH

namespace Dumux
{
/*!
 * \ingroup TwoPNCMinniModel
 */
// \{

/*!
 * \brief Enumerates the formulations which the 2p2c model accepts.
 */

/*!
 * \brief The indices for the isothermal TwoPNCMin model.
 *
 * \tparam formulation The formulation, either pwSn or pnSw.
 * \tparam PVOffset The first index in a primary variable vector.
 */
template <class TypeTag, int PVOffset=0>
class TwoPNCMinNIIndices: public TwoPNCMinIndices<TypeTag, PVOffset>
{
	typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int temperatureIdx = PVOffset + FluidSystem::numComponents + FluidSystem::numSPhases; //! The index for temperature in primary variable vectors.
    static const int energyEqIdx = PVOffset + FluidSystem::numComponents + FluidSystem::numSPhases; //! The index for energy in equation vectors.
};
}
#endif
