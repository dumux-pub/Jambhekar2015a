// $Id: 2p2cproperties.hh 4952 2011-01-04 15:49:28Z lauser $
/*****************************************************************************
 *   Copyright (C) 2008-2010 by Andreas Lauser                               *
 *   Copyright (C) 2008-2009 by Melanie Darcis                               *
 *   Copyright (C) 2008-2009 by Klaus Mosthaf                                *
 *   Copyright (C) 2008-2009 by Bernd Flemisch                               *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPNCMinModel
 */
/*!
 * \file
 *
 * \brief Defines the properties required for the 2pncMin BOX model.
 */
#ifndef DUMUX_2PNCMINNI_PROPERTIES_HH
#define DUMUX_2PNCMINNI_PROPERTIES_HH

#include <dumux/implicit/2pncmin/2pncminproperties.hh>

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tag for the isothermal two phase n component non-isothermal mineralisation problems
NEW_TYPE_TAG(BoxTwoPNCMinNI, INHERITS_FROM(BoxTwoPNCMin));

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////
NEW_PROP_TAG(TwoPNCMinNIIndices); //!< DEPRECATED Enumerations for the 2pncnimin models

NEW_PROP_TAG(ThermalConductivityModel); //!< The model for the effective thermal conductivity
}
}

#endif
