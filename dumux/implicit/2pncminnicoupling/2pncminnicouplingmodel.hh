/*****************************************************************************
 *   Copyright (C) 2008-2011 by Klaus Mosthaf                                *
 *   Copyright (C) 2008-2009 by Andreas Lauser                               *
 *   Copyright (C) 2008 by Bernd Flemisch                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Adaption of the BOX scheme to the non-isothermal two-phase two-component flow model.
 */
#ifndef DUMUX_2PNCMINNI_COUPLING_MODEL_HH
#define DUMUX_2PNCMINNI_COUPLING_MODEL_HH

#include <dumux/implicit/2pncminni/2pncminnimodel.hh>

namespace Dumux {

/*!
 * \ingroup ImplicitModels
 */

/*!
 * \ingroup TwoPTwoCNIModel
 * \brief Adaption of the BOX scheme to the non-isothermal two-phase two-component flow model.
 *
 * Here, only the 2p2cnicouplingpropertydefaults.hh are included.
 */
template<class TypeTag>
class TwoPNCMinNICouplingModel : public TwoPNCMinNIModel<TypeTag>
{};
}

#include "2pncminnicouplingpropertydefaults.hh"

#endif
