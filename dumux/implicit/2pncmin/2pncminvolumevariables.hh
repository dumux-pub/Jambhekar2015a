// $Id:2p2cvolumevariables.hh 5151 2011-02-01 14:22:03Z lauser $
/*****************************************************************************
 *   Copyright (C) 2008,2009 by Vishal Jambhekar,
 * 								Alexzander Kissinger,
 * 								Klaus Mosthaf,                               *
 *                              Andreas Lauser,                              *
 *                              Bernd Flemisch                               *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Contains the quantities which are constant within a
 *        finite volume in the two-phase, n-component mineralisation model.
 */
#ifndef DUMUX_2PNCMin_VOLUME_VARIABLES_HH
#define DUMUX_2PNCMin_VOLUME_VARIABLES_HH



#include <dumux/implicit/common/implicitmodel.hh>
#include <dumux/material/fluidstates/compositionalfluidstate.hh>
#include <dumux/common/math.hh>
#include <dune/common/collectivecommunication.hh>
#include <vector>
#include <iostream>

#include "2pncminproperties.hh"
#include "2pncminindices.hh"
#include <dumux/material/constraintsolvers/computefromreferencephase2pnc.hh>
#include <dumux/material/constraintsolvers/miscible2pnccomposition.hh>
#include <dumux/implicit/2pnc/2pncvolumevariables.hh>

namespace Dumux
{

/*!
 * \ingroup TwoPNCMinModel
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the two-phase, n-component mineralisation model.
 */
template <class TypeTag>
class TwoPNCMinVolumeVariables : public TwoPNCVolumeVariables<TypeTag>
{
    typedef TwoPNCVolumeVariables<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
//     typedef typename GET_PROP_TYPE(TypeTag, Chemistry) Chemistry;

    enum {
        dim = GridView::dimension,
        dimWorld=GridView::dimensionworld,

        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numSPhases =  GET_PROP_VALUE(TypeTag, NumSPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),
        numMajorComponents = GET_PROP_VALUE(TypeTag, NumMajorComponents),

        // formulations
        formulation = GET_PROP_VALUE(TypeTag, Formulation),
        plSg = TwoPNCFormulation::plSg,
        pgSl = TwoPNCFormulation::pgSl,

        // phase indices
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,

        // component indices
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,

        // phase presence enums
        nPhaseOnly = Indices::nPhaseOnly,
        wPhaseOnly = Indices::wPhaseOnly,
        bothPhases = Indices::bothPhases,

        // primary variable indices
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,
	
		useSalinity = GET_PROP_VALUE(TypeTag, useSalinity)
    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef typename Grid::ctype CoordScalar;
    typedef Dumux::Miscible2pNcComposition<Scalar, FluidSystem> Miscible2pNcComposition;
    typedef Dumux::ComputeFromReferencePhase2pNc<Scalar, FluidSystem> ComputeFromReferencePhase2pNc;

public:
  
      //! The type of the object returned by the fluidState() method
      typedef CompositionalFluidState<Scalar, FluidSystem> FluidState;
    /*!
     * \brief Update all quantities for a given control volume.
     *
     * \param primaryVariables The primary variables
     * \param problem The problem
     * \param element The element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param scvIdx The local index of the SCV (sub-control volume)
     * \param isOldSol Evaluate function with solution of current or previous time step
     */
    void update(const PrimaryVariables &priVars,
                const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                bool isOldSol)
    {
        ParentType::update(priVars,
                           problem,
                           element,
                           fvGeometry,
                           scvIdx,
                           isOldSol);
	
        completeFluidState(priVars, problem, element, fvGeometry, scvIdx, this->fluidState_, isOldSol);
	
	    /////////////
        // calculate the remaining quantities
        /////////////
	
    // porosity and solidity evaluation
	InitialPorosity_ = problem.spatialParams().porosity(element,
							    fvGeometry,
							    scvIdx);
	
	//InitialPermeability_ = problem.spatialParams().intrinsicPermeability(element,
	//								     fvGeometry,
	//								     scvIdx);

	totalPrecip_ = 0.0;
    for(int sPhaseIdx = 0; sPhaseIdx < numSPhases; ++sPhaseIdx)
    {
    	solidity_[sPhaseIdx] = priVars[numComponents + sPhaseIdx];
    	totalPrecip_+= solidity_[sPhaseIdx];
    }
    
// TODO/FIXME: The salt crust porosity is not clearly defined. However form literature review it is
//    found that the salt crust have porosity of approx. 10 %. Thus we restrict the decrease in porosity
//    to this limit. Moreover in the Problem files the precipitation should also be made dependent on local
//    porosity value, as the porous media media properties change related to salt precipitation will not be
//    accounted otherwise.

//      this->porosity_ = InitialPorosity_ - totalPrecip_;

   this->porosity_ = std::max(0.1, std::max(0.0, InitialPorosity_ - totalPrecip_));

	salinity_= 0.0;
	moleFracSalinity_ = 0.0;
	for (int compIdx = numMajorComponents; compIdx< numComponents; compIdx++)	//sum of the mass fraction of the components
	{
		if(this->fluidState_.moleFraction(wPhaseIdx, compIdx)> 0)
		{
			salinity_+= this->fluidState_.massFraction(wPhaseIdx, compIdx);
			moleFracSalinity_ += this->fluidState_.moleFraction(wPhaseIdx, compIdx);
		}
	}
	
//     for(int sPhaseIdx = 0; sPhaseIdx < numSPhases; ++sPhaseIdx)
//     {
//         omega_[sPhaseIdx] = Chemistry::omega(sPhaseIdx, this->fluidState_, moleFracSalinity_);
//     }

// TODO/FIXME: Different relations for the porosoty-permeability changes are given here. We have to fins a way
//    so that one can select the relation form the input file.

	// kozeny-Carman relation
 	permFactor_  =  std::pow(((1-InitialPorosity_)/(1-this->porosity_)),2)
			* std::pow((this->porosity_/InitialPorosity_),3);

	// Verma-Pruess relation
//	permFactor_  =  100 * std::pow(((this->porosity_/InitialPorosity_)-0.9),2);

	// Modified Fair-Hatch relation with final porosity set to 0.2 and E1=1
//	permFactor_  =  std::pow((this->porosity_/InitialPorosity_),3)
//	       * std::pow((std::pow((1 - InitialPorosity_),2/3))+(std::pow((0.2 - InitialPorosity_),2/3)),2)
//	       / std::pow((std::pow((1 -this->porosity_),2/3))+(std::pow((0.2 -this->porosity_),2/3)),2);

	//Timur relation with residual water saturation set to 0.001
//    permFactor_ =  0.136 * (std::pow(this->porosity_,4.4)) / (2000 * (std::pow(0.001,2)));

	//Timur relation1 with residual water saturation set to 0.001
//    permFactor_ =  0.136 * (std::pow(this->porosity_,4.4)) / (200000 * (std::pow(0.001,2)));


    //Bern. relation
   // permFactor_ = std::pow((this->porosity_/InitialPorosity_),8);

    //Tixier relation with residual water saturation set to 0.001
    //permFactor_ = (std::pow((250 * (std::pow(this->porosity_,3)) / 0.001),2)) / InitialPermeability_;

	//Coates relation with residual water saturation set to 0.001
    //permFactor_ = (std::pow((100 * (std::pow(this->porosity_,2)) * (1-0.001) / 0.001,2))) / InitialPermeability_ ;

				
	// energy related quantities not contained in the fluid state
    //asImp_().updateEnergy_(priVars, problem,element, fvGeometry, scvIdx, isOldSol);
    }

      /*!
    * \copydoc BoxModel::completeFluidState
    * \param isOldSol Specifies whether this is the previous solution or the current one
    */
  static void completeFluidState(const PrimaryVariables& priVars,
				  const Problem& problem,
				  const Element& element,
				  const FVElementGeometry& fvGeometry,
				  int scvIdx,
				  FluidState& fluidState,
				  bool isOldSol = false)

    {
        Scalar t = Implementation::temperature_(priVars, problem, element,fvGeometry, scvIdx);
        fluidState.setTemperature(t);

        int globalVertIdx = problem.model().dofMapper().map(element, scvIdx, dim);
        int phasePresence = problem.model().phasePresence(globalVertIdx, isOldSol);

	/////////////
        // set the saturations
        /////////////

	Scalar Sg;
        if (phasePresence == nPhaseOnly)
            Sg = 1.0;
        else if (phasePresence == wPhaseOnly) {
            Sg = 0.0;
        }
        else if (phasePresence == bothPhases) {
            if (formulation == plSg)
                Sg = priVars[switchIdx];
            else if (formulation == pgSl)
                Sg = 1.0 - priVars[switchIdx];
            else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");
        }
	else DUNE_THROW(Dune::InvalidStateException, "phasePresence: " << phasePresence << " is invalid.");
	    fluidState.setSaturation(nPhaseIdx, Sg);
        fluidState.setSaturation(wPhaseIdx, 1.0 - Sg);

	        /////////////
        // set the pressures of the fluid phases
        /////////////

        // calculate capillary pressure
        const MaterialLawParams &materialParams = problem.spatialParams().materialLawParams(element, fvGeometry, scvIdx);
        Scalar pc = MaterialLaw::pc(materialParams, 1 - Sg);

        // extract the pressures
        if (formulation == plSg) {
            fluidState.setPressure(wPhaseIdx, priVars[pressureIdx]);
            fluidState.setPressure(nPhaseIdx, priVars[pressureIdx] + pc);
        }
        else if (formulation == pgSl) {
            fluidState.setPressure(nPhaseIdx, priVars[pressureIdx]);
            fluidState.setPressure(wPhaseIdx, priVars[pressureIdx] - pc);
        }
        else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");

	    /////////////
        // calculate the phase compositions
        /////////////

	typename FluidSystem::ParameterCache paramCache;

        // now comes the tricky part: calculate phase composition
        if (phasePresence == bothPhases) {
            // both phases are present, phase composition results from
            // the gas <-> liquid equilibrium. This is
            // the job of the "MiscibleMultiPhaseComposition"
            // constraint solver

        	// set the known mole fractions in the fluidState so that they
        	// can be used by the Miscible2pNcComposition constraint solver
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
            	fluidState.setMoleFraction(wPhaseIdx, compIdx, priVars[compIdx]);
            }

            Miscible2pNcComposition::solve(fluidState,
					    paramCache,
					    wPhaseIdx,	//known phaseIdx
					    /*setViscosity=*/true,
					    /*setInternalEnergy=*/false);
        }
        else if (phasePresence == nPhaseOnly){

            Dune::FieldVector<Scalar, numComponents> moleFrac;
            Dune::FieldVector<Scalar, numComponents> fugCoeffL;
            Dune::FieldVector<Scalar, numComponents> fugCoeffG;

            for (int compIdx=0; compIdx<numComponents; ++compIdx)
            {
            	fugCoeffL[compIdx] = FluidSystem::fugacityCoefficient(fluidState,
								      paramCache,
								      wPhaseIdx,
								      compIdx);
            	fugCoeffG[compIdx] = FluidSystem::fugacityCoefficient(fluidState,
								      paramCache,
								      nPhaseIdx,
								      compIdx);
            }
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            	moleFrac[compIdx] = (priVars[compIdx]*fugCoeffL[compIdx]*fluidState.pressure(wPhaseIdx))
				    /(fugCoeffG[compIdx]*fluidState.pressure(nPhaseIdx));
            
            moleFrac[wCompIdx] =  priVars[switchIdx];
            Scalar sumMoleFracNotGas = 0;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
            		sumMoleFracNotGas+=moleFrac[compIdx];
            }
            sumMoleFracNotGas += moleFrac[wCompIdx];
            moleFrac[nCompIdx] = 1 - sumMoleFracNotGas;

			typedef Dune::FieldMatrix<Scalar, numComponents, numComponents> Matrix;
			typedef Dune::FieldVector<Scalar, numComponents> Vector;


            // Set fluid state mole fractions
            for (int compIdx=0; compIdx<numComponents; ++compIdx)
            {
            	fluidState.setMoleFraction(nPhaseIdx, compIdx, moleFrac[compIdx]);
            }

            // calculate the composition of the remaining phases (as
            // well as the densities of all phases). this is the job
            // of the "ComputeFromReferencePhase2pNc" constraint solver
            ComputeFromReferencePhase2pNc::solve(fluidState,
                                             paramCache,
                                             nPhaseIdx,
                                             /*setViscosity=*/true,
                                             /*setInternalEnergy=*/false);

	     }
        else if (phasePresence == wPhaseOnly){
        
	    // only the liquid phase is present, i.e. liquid phase
	    // composition is stored explicitly.
	    // extract _mass_ fractions in the gas phase
            Dune::FieldVector<Scalar, numComponents> moleFrac;

            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
            	moleFrac[compIdx] = priVars[compIdx];
            }
            moleFrac[nCompIdx] = priVars[switchIdx];
            Scalar sumMoleFracNotWater = 0;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
            		sumMoleFracNotWater+=moleFrac[compIdx];
            }
            sumMoleFracNotWater += moleFrac[nCompIdx];
            moleFrac[wCompIdx] = 1 -sumMoleFracNotWater;

//             convert mass to mole fractions and set the fluid state
            for (int compIdx=0; compIdx<numComponents; ++compIdx)
            {
            	fluidState.setMoleFraction(wPhaseIdx, compIdx, moleFrac[compIdx]);
            }

//             calculate the composition of the remaining phases (as
//             well as the densities of all phases). this is the job
//             of the "ComputeFromReferencePhase2pNc" constraint solver
            ComputeFromReferencePhase2pNc::solve(fluidState,
                                             paramCache,
                                             wPhaseIdx,
                                             /*setViscosity=*/true,
                                             /*setInternalEnergy=*/false);
        }
        paramCache.updateAll(fluidState);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            Scalar h = Implementation::enthalpy_(fluidState, paramCache, phaseIdx);
            fluidState.setEnthalpy(phaseIdx, h);
        }
    }

    Scalar omega(int phaseIdx) const
    { return omega_[phaseIdx - numPhases]; }
    
    Scalar solidity(int phaseIdx) const
    { return solidity_[phaseIdx - numPhases]; }

    Scalar InitialPorosity() const
    { return InitialPorosity_;}

    Scalar InitialPermeability() const
    { return InitialPermeability_;}

    Scalar permFactor() const
    { return permFactor_; }

    Scalar moleFraction(int phaseIdx, int compIdx) const
    {
       return this->fluidState_.moleFraction(phaseIdx, compIdx);
    }

    Scalar moleFracSalinity() const
    {
    	return moleFracSalinity_;
    }

    Scalar salinity() const
    {
    	return salinity_;
    }

	Scalar density(int phaseIdx) const
    {
    	if (phaseIdx < numPhases)
    		return this->fluidState_.density(phaseIdx);
#if SALINIZATION
    	else if (phaseIdx >= numPhases)
			return FluidSystem::saltDensity(phaseIdx);
#endif
    	else
    		DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }
	/*!
	 * \brief Returns the liquid vapor pressure within the control volume.
	 */
#if SALINIZATION
	Scalar vaporPressure() const
	{ return FluidSystem::vaporPressure(this->fluidState_.temperature(/*phaseIdx=*/0),this->fluidState_.moleFraction(wPhaseIdx, FluidSystem::NaClIdx)); }
#endif

    /*!
     * \brief Returns the mass density of a given phase within the
     *        control volume.
     *
     * \param phaseIdx The phase index
     */
    Scalar molarDensity(int phaseIdx) const
    {
    	if (phaseIdx < numPhases)
    		return this->fluidState_.molarDensity(phaseIdx);
#if SALINIZATION
    	else if (phaseIdx >= numPhases)
    		return FluidSystem::saltMolarDensity(phaseIdx);
#endif
    	else
        	DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }
    
#if SALINIZATION
     Scalar molality(int phaseIdx, int compIdx) const // [moles/Kg]
    { return this->fluidState_.moleFraction(phaseIdx, compIdx)/FluidSystem::molarMass(compIdx);}
#endif
protected:
	friend class TwoPNCVolumeVariables<TypeTag>;
	static Scalar temperature_(const PrimaryVariables &priVars,
							   const Problem& problem,
							   const Element &element,
							   const FVElementGeometry &fvGeometry,
							   int scvIdx)
    {
        return problem.temperatureAtPos(fvGeometry.subContVol[scvIdx].global);
    }
    
    template<class ParameterCache>
    static Scalar enthalpy_(const FluidState& fluidState,
                            const ParameterCache& paramCache,
                            int phaseIdx)
    {
        return 0;
    }
	
	
	
	    /*!
         * \brief Update all quantities for a given control volume.
         *
         * \param priVars The solution primary variables
         * \param problem The problem
         * \param element The element
         * \param fvGeometry Evaluate function with solution of current or previous time step
         * \param scvIdx The local index of the SCV (sub-control volume)
         * \param isOldSol Evaluate function with solution of current or previous time step
         */
        void updateEnergy_(const PrimaryVariables &priVars,
                           const Problem &problem,
                           const Element &element,
                           const FVElementGeometry &fvGeometry,
                           const int scvIdx,
                           bool isOldSol)
        { };

    Scalar solidity_[numSPhases];
    Scalar omega_[numSPhases];
    Scalar permFactor_;
    Scalar InitialPorosity_;
	Scalar InitialPermeability_;
    Scalar totalPrecip_;
	Scalar salinity_;
	Scalar moleFracSalinity_;

private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }
};

} // end namespace

#endif
