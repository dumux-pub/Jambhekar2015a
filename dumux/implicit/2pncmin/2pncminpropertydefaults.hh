// $Id: 2p-3c-Minproperties.hh 3784 2010-06-24 13:43:57Z bernd $
/*****************************************************************************
 *   Copyright (C) 2008 by Klaus Mosthaf, Andreas Lauser, Bernd Flemisch     *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPNCMinModel
 */
/*!
 * \file
 *
 * \brief Defines default values for most properties required by the
 *        2pncMin box model.
 */
#ifndef DUMUX_2PNCMIN_PROPERTY_DEFAULTS_HH
#define DUMUX_2PNCMIN_PROPERTY_DEFAULTS_HH

#include "2pncminindices.hh"
#include "2pncminmodel.hh"
#include "2pncminindices.hh"
#include "2pncminfluxvariables.hh"
#include "2pncminvolumevariables.hh"
#include "2pncminproperties.hh"

//#include "2pncminnewtoncontroller.hh"
#include <dumux/implicit/2pnc/2pncnewtoncontroller.hh>
#include <dumux/implicit/common/implicitdarcyfluxvariables.hh>
#include <dumux/material/spatialparams/implicitspatialparams.hh>

namespace Dumux
{

namespace Properties {
//////////////////////////////////////////////////////////////////
// Property values
//////////////////////////////////////////////////////////////////

/*!
 * \brief Set the property for the number of components.
 *
 * We just forward the number from the fluid system
 *
 */

SET_PROP(BoxTwoPNCMin, NumSecComponents)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numSecComponents;

};

/*!
 * \brief Set the property for the number of solid phases.
 *
 * We just forward the number from the fluid system
 *
 */
SET_PROP(BoxTwoPNCMin, NumSPhases)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numSPhases;
};

/*!
 * \brief Set the property for the number of equations. For each component and each mineral phase one equation has to
 * be solved.
 */
SET_PROP(BoxTwoPNCMin, NumEq)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numComponents + FluidSystem::numSPhases;
};


//! Use the 2p-3c-Min local jacobian operator for the 2p-3c-Min model
SET_TYPE_PROP(BoxTwoPNCMin,
              LocalResidual,
              TwoPNCMinLocalResidual<TypeTag>);

//! the Model property
SET_TYPE_PROP(BoxTwoPNCMin, Model, TwoPNCMinModel<TypeTag>);

//! the VolumeVariables property
SET_TYPE_PROP(BoxTwoPNCMin, VolumeVariables, TwoPNCMinVolumeVariables<TypeTag>);

//! the FluxVariables property
SET_TYPE_PROP(BoxTwoPNCMin, FluxVariables, TwoPNCMinFluxVariables<TypeTag>);

//! The indices required by the isothermal 2p-3c-Min model
SET_TYPE_PROP(BoxTwoPNCMin, Indices, TwoPNCMinIndices <TypeTag, /*PVOffset=*/0>);

// disable useSalinity for the calculation of osmotic pressure by default
SET_BOOL_PROP(BoxTwoPNCMin, useSalinity, false);


//! default value for the forchheimer coefficient
// Source: Ward, J.C. 1964 Turbulent flow in porous media. ASCE J. Hydraul. Div 90.
//        Actually the Forchheimer coefficient is also a function of the dimensions of the
//        porous medium. Taking it as a constant is only a first approximation
//        (Nield, Bejan, Convection in porous media, 2006, p. 10)
SET_SCALAR_PROP(BoxModel, SpatialParamsForchCoeff, 0.55);

//
}

}

#endif
